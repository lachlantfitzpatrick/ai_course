package a1;

import java.util.LinkedList;

/**
 * Contains all the information required to run a query on a graph. This
 * class is immutable.
 * <p>
 * Created by lachlan on 21/08/16.
 */
public class Query {

    /*
        class invariant:
            graph.contains(initialNode) &&
            graph.contains(goalNode)
     */

    // The graph the query is to be run on.
    private Graph environment;
    // The initial and goal nodes.
    private Node initialNode, goalNode;

    // The algorithm to be used to perform the expand.
    private Search searchAlgorithm;

    /**
     * Creates a query with a given environment, initial node, goal node and
     * expand algorithm.
     */
    public Query(Graph environment, Node initialNode, Node goalNode,
                 Search searchAlgorithm) {
        this.environment = environment;
        this.initialNode = initialNode;
        this.goalNode = goalNode;
        this.searchAlgorithm = searchAlgorithm;
    }

    /**
     * Performs the query and returns the output in string format ready for
     * user interpretation.
     */
    public StringBuffer run() {
        LinkedList<Node> path;
        // Run the correct algorithm.
        switch (searchAlgorithm) {
            case UNIFORM_COST:
                path = UniformSearch.search(environment, initialNode, goalNode);
                break;
            case A_STAR:
                path = AStarSearch.search(environment, initialNode, goalNode);
                break;
            default:
                throw new InvalidSearchException("Invalid expand algorithm: " +
                        searchAlgorithm.toString());
        }
        // Format path ready to be outputted.
        return formatPath(path);
    }

    /**
     * Formats a path in LinkedList to a String.
     */
    private StringBuffer formatPath(LinkedList<Node> path){
        StringBuffer outputString = new StringBuffer("");
        if (path.size() >= 1) {
            outputString.append(path.get(0));
        }
        for(int i = 1; i < path.size(); i++) {
            outputString.append("-");
            outputString.append(path.get(i));
        }
        double cost = path.get(path.size()-1).getCost();
        String temp = " cost %.2f at depth %d\n";
        temp = String.format(temp, cost, path.size() - 1);
        outputString.append(temp);
        System.out.print(outputString);

        return outputString;
    }

}
