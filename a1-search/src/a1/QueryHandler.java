package a1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.io.FileWriter;

/**
 * Created by swagahashi on 23/08/16.
 *
 * Runs a series of queries.
 * <p>
 */
public class QueryHandler {

    /**
     * Runs a series of queries in the order that they appear in the input list.
     */
    public static void runQueries(Graph graph, ArrayList<Query> queries,
                                  String outputFilename) {
        //TODO insert the query output into the output file.
        //TODO reset all the nodes.

        //Run the Queries
        try {
            File outputFile = new File(outputFilename);
            FileWriter fw = new FileWriter(outputFile);
            for (Query query : queries) {
                long startTime = System.currentTimeMillis();
                graph.resetNodes();
                StringBuffer queryOutput = query.run();
                System.out.println("Query time: " + (System.currentTimeMillis()-startTime) + "ms");
                outputFile.createNewFile();
                fw.write(queryOutput.toString());
            }
            fw.close();
            // Reset all the nodes
        } catch (IOException ioe) {
            System.err.print("Error in saving output!");
        }
    }
}
