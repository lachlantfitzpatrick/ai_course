package a1;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * A priority queue for storing the fringe nodes to be expanded.
 * <p>
 * Created by lachlan on 16/08/16.
 */
public class NodePriorityQueue implements Iterable<Node> {

    /*
        class invariant:
            there is only ever one instance of node, n, in the queue &&
            the ordering of the queue is from lowest cost to highest cost
     */

    // The queue. LinkedList used in preference to ArrayList as we will often
    // be removing the first element from list.
    private ArrayList<Node> queue;

    /**
     * Creates a a1.NodePriorityQueue with an empty queue.
     */
    public NodePriorityQueue(int capacity) {
        queue = new ArrayList<>(capacity);
    }

    /**
     * Adds a node into the queue.
     *
     * @require the cost of newFringe has been set before passing it in
     */
    public void add(Node newFringe) {
        // Skip recursion.
        if (queue.size() == 0) {
            newFringe.queue();
            queue.add(0, newFringe);
            return;
        }
        // Otherwise down the recursion hole we go.
        int index = getInsertIndex(newFringe.getTotalCost(), 0, queue.size() -
                1);
        newFringe.queue();
        queue.add(index, newFringe);
    }

    /**
     * Updates a node's position that is already in the queue.
     */
    public void updateNode(Node newFringe, double newCost, Node parent) {
        remove(newFringe);
        newFringe.setCost(newCost);
        newFringe.setParent(parent);
        newFringe.setHeuristicCost();
        add(newFringe);
    }

    /**
     * Removes the node from queue in log(n) time.
     */
    public void remove(Node node) {
//        int index = indexOf(node);
        int index = queue.indexOf(node);
        if(index != -1){
            queue.remove(index);
        }
        else {
            System.out.println("wtf");
        }
        node.unqueue();
    }

    /**
     * Returns the index of node in queue or -1 if empty.
     */
    public int indexOf(Node node) {
        if (queue.size() == 0) {
            return -1;
        }
        return indexOf(node, node.getCost(), 0, queue.size() - 1);
    }

    /**
     * Recursively finds the index of node. The inputted cost is the cost of
     * the node.
     */
    private int indexOf(Node node, double cost, int start, int end) {
        int newIndex = (start + end) / 2;
        // Base case.
        if(newIndex == queue.size()) {
            return -1;
        }
        if (node.equals(queue.get(newIndex))) {
            return newIndex;
        } else if (start == end) {
            return -1;
        }
        double indexNodeCost = queue.get(newIndex).getCost();
        // Case where we are in earlier part of list section.
        if (indexNodeCost > cost) {
            return indexOf(node, cost, start, newIndex);
        }
        // Case where we are in later part of list section.
        // Case: if(indexNodeCost < cost)
        else {
            return indexOf(node, cost, newIndex + 1, end);
        }

    }

    /**
     * Recursively finds the index that a node with cost, cost, will be
     * inserted at.
     */
    private int getInsertIndex(double cost, int start, int end) {
        int newIndex = (start + end) / 2;
        // Base case.
        if (newIndex == start && newIndex == end) {
            if (queue.get(newIndex).getCost() < cost) {
                return newIndex + 1;
            }
            return newIndex;
        }
        double indexNodeCost = queue.get(newIndex).getTotalCost();
        // Case where we are in earlier part of list section.
        if (indexNodeCost >= cost) {
            return getInsertIndex(cost, start, newIndex);
        }
        // Case where we are in later part of list section.
        // Case: if(indexNodeCost < cost)
        else {
            return getInsertIndex(cost, newIndex + 1, end);
        }
    }

    /**
     * Checks whether the queue is empty.
     */
    public boolean isEmpty() {
        return queue.isEmpty();
    }

    /**
     * Pops the next element off the list.
     */
    public Node pop() {
        Node next = queue.remove(0);
        next.unqueue();
        return next;
    }

    /**
     * Checks whether queue contains node.
     */
    public boolean contains(Node node) {
        return queue.contains(node);
    }


    @Override
    public Iterator<Node> iterator() {
        return queue.iterator();
    }
}
