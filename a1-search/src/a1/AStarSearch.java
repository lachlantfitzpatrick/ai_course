package a1;

import java.util.Set;
import java.util.LinkedList;

/**
 * Created by lachlan on 16/08/16.
 */
public class AStarSearch {

    private static NodePriorityQueue queue;

    /**
     * Takes a graph, start node and end node and uses a uniform cost setSearched
     * algorithm to determine the optimal path through the graph from the
     * initial to goal nodes. This path is then returned as a list.
     */
    public static LinkedList<Node> search(Graph graph, Node initial, Node
            goal) {
        // Set the cost of initial to 0 as this is where we start.
        initial.setCost(0.0);
        initial.updateHeuristic(0.0);
        initial.setSearchAStar();

        // Setup the priority queue to hold the fringe nodes.
        queue = new NodePriorityQueue(graph.getVertexCount());
        queue.add(initial);

        // Perform the setSearched while there are nodes in the queue.
        Node current;
        while (!queue.isEmpty()) {
            current = queue.pop();

            // Exit if we are at the goal.
            if (current.equals(goal)) {
                return generatePath(goal);
            }

            // Add the connections of current to queue.
            Set<Node> connections = current.getConnections();
            for (Node connection : connections) {
                connection.setSearchAStar();
                queueConnection(connection, current,
                        current.getConnectionCost(connection));
            }

        }
        return null;
    }

    /**
     * Checks a new fringe node against searched fringe nodes to check if the
     * new fringe node has already been used and if so determines whether to
     * update the node with a new parent and cost. Then adds the node to the
     * queue.
     */
    private static void queueConnection(Node newFringe, Node parent,
                                        Double moveCost) {
        // If the newFringe didn't exist in searched, add it to queue and
        // searched list.
        if (!newFringe.isSearched()) {
            // Update all the appropriate data structures.
            newFringe.setParent(parent);
            newFringe.setCost(moveCost + parent.getCost());
            newFringe.setHeuristicCost();
            newFringe.setSearched();
            queue.add(newFringe);
            return;
        }
        // If the newFringe did exist in searched we need to compare the
        // costs and determine whether it should be replaced.
        else {
            // At this point newFringe == oldNode and will have its cost set.
            double newCost = parent.getCost() + moveCost;
            if (newFringe.getCost() > newCost) {
                queue.remove(newFringe);
                newFringe.setCost(parent.getCost() + moveCost);
                newFringe.setHeuristicCost();
                newFringe.setParent(parent);
                queue.add(newFringe);
                return;
            }
        }
    }

    /**
     * Generates a LinkedList of the optimal path given the goal node.
     *
     * @requires the setSearched algorithm must have found a solution.
     */
    private static LinkedList<Node> generatePath(Node goal) {
        LinkedList<Node> path = new LinkedList<>();
        path.add(0, goal);

        Node parent = goal.getParent();
        while (parent != null) {
            path.add(0, parent);
            parent = parent.getParent();
        }
        return path;
    }

}
