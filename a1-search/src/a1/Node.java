package a1;

import java.util.HashMap;
import java.util.Set;

/**
 * A node on the graph. A node is responsible for knowing its connection and
 * their respective costs, the node that was used to move onto this node and
 * the cost to get from the initial node of a expand to this node if this
 * node has been searched.
 * <p>
 * Created by lachlan on 16/08/16.
 */
public class Node {

    /*
        Class invariant:
            if(!searched){cost == null && parent == null}
     */

    // The ID of this node. This ID should be unique in any graph.
    private int ID; // Immutable

    // The nodes connected to this node and the cost to move to the nodes.
    private HashMap<Node, Double> connections;
    // The node that was used to move onto this node.
    private Node parent = null;

    // Whether the node has been searched.
    private boolean searched = false;
    // Whether the node is currently queued.
    private boolean queued = false;
    // The cost of getting from the initial node to this node.
    private Double cost = null;

    // This node's heuristic.
    private Double heuristic = null;
    // The heuristic cost of getting from the initial node to this node.
    private Double heuristicCost = null;

    // Flag to indicate whether we are performing a uniform or a star search.
    private boolean isUniform;
    /**
     * Creates a node with an ID and initialises variables.
     */
    public Node(int ID) {
        this.ID = ID;
        connections = new HashMap<>();
    }

    /**
     * Calculates and sets this node's heuristic.
     */
    public void calculateHeuristic(Double costToThis) {

    }

    /**
     * Updates the value of this node's heuristic
     */

    public void updateHeuristic(Double cost) {
        try {
            if (cost < heuristic) {
                heuristic = cost;
            }
        } catch (NullPointerException npe) {
            heuristic = cost;
        }
    }

    /**
     * Gets the total cost to this node.
     */
    public double getTotalCost() {
        return isUniform ? getCost() : getHeuristicCost();
    }

    /**
     * Sets the flag to indicate a uniform search.
     */
    public void setSearchUniform() {
        isUniform = true;
    }

    /**
     * Sets the flag to indicate an A star search.
     */
    public void setSearchAStar() {
        isUniform = false;
    }

    /**
     * Returns the state of the search flag.
     */
    public boolean isUniform() {
        return isUniform;
    }

    /**
     * Gets the value of the heuristic for this node.
     */
    public Double getHeuristic() {
        return heuristic;
    }

    /**
     * Compares incoming node cost with current node cost
     * and sets the node's minimum cost variable accordingly.
     * Then, adds a connection.
     *
     *
     * @require !connections.contains(connection)
     */
    public void addConnection(Node connection, Double cost) {
        updateHeuristic(cost);
        //heuristic = 0.0;
        connections.put(connection, cost);
    }

    /**
     * Get the keySet for connections.
     */
    public Set<Node> getConnections() {
        return connections.keySet();
    }

    /**
     * Get connection cost.
     */
    public Double getConnectionCost(Node connection){
        Double cost = connections.get(connection);
        if(cost == null){
            throw new NodeNotFoundException("No connection to node " +
                    connection.toString());
        }
        return cost;
    }

    /**
     * Sets the parent.
     */
    public void setParent(Node parent) {
        this.parent = parent;
    }

    /**
     * Gets the parent node.
     */
    public Node getParent() {
        return parent;
    }

    /**
     * Gets the parent's ID.
     */
    public int getParentID() {
        return parent.getID();
    }

    /**
     * Returns the ID of this node.
     */
    protected int getID() {
        return ID;
    }

    /**
     * Sets the cost.
     */
    public void setCost(Double cost){
        this.cost = cost;
    }

    /**
     * Returns the cost of this.
     */
    public Double getCost() {
        return cost;
    }


    /**
     * Sets the heuristic cost of this
     */

    public void setHeuristicCost() {
        if(!isUniform) {
            this.heuristicCost = getHeuristic() + getCost();
        }
    }

    /**
     * Returns the real cost (cost + heuristic) of this.
     */
    public Double getHeuristicCost() {
        return heuristicCost;
    }

    /**
     * Sets this node as searched.
     */
    public void setSearched() {
        searched = true;
    }

    /**
     * Gets the searched status of this node.
     */
    public boolean isSearched() {
        return searched;
    }

    /**
     * Sets the flag to indicate that this is queued.
     */
    public void queue() {
        this.queued = true;
        this.setSearched();
    }

    /**
     * Sets the flag to indicate that this is not queued.
     */
    public void unqueue() {
        this.queued = false;
    }

    /**
     * Returns the status of queued.
     */
    public boolean isQueued() {
        return queued;
    }

    /**
     * Resets this node (as if no expand has occurred.
     */
    public void resetNode() {
        searched = false;
        queued = false;
        parent = null;
        cost = null;
    }

    /**
     * Overrides the hashcode method of Object. Used for creating the graph.
     */
    @Override
    public int hashCode() {
        int hash = 19;
        hash = hash * 65537 + ID;
        return hash * 65537;
    }


    /**
     * Checks whether the given object is a node and has the same ID as this.
     */
    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Node)) {
            return false;
        }
        Node node = (Node) object;
        if (node.getID() == this.ID) {
            return true;
        }
        return false;
    }

    /**
     * The string representation of a a1.Node is just its ID.
     */
    @Override
    public String toString(){
        return Integer.toString(ID);
    }
}
