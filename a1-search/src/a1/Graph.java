package a1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * A graph of nodes. This groups a collection of nodes into a graph.
 * <p>
 * Created by lachlan on 16/08/16.
 */
public class Graph {

    // The number of vertices in the graph.
    private int vertexCount;

    // The list of all the nodes in this graph.
    private HashMap<Integer, Node> nodes;

    /**
     * Initialise the graph with an empty list.
     */
    public Graph(int vertexCount) {
        this.vertexCount = vertexCount;
        nodes = new HashMap<>();
    }

    /**
     * Gets the number of vertices on the graph.
     */
    public int getVertexCount() {
        return this.vertexCount;
    }

    /**
     * Adds a node to the graph. It is assumed that connections have already
     * been made for the node.
     */

    public void resetNodes() {
        Set<Integer> keys = nodes.keySet();
        for(Integer key : keys) {
            nodes.get(key).resetNode();
        }
//        for(int i = 1; i <= nodes.size(); i++) {
//            nodes.get(i).resetNode();
//        }
    }

    public void addNode(Node node) {
        nodes.put(node.getID(), node);
    }

    /**
     * Get node by ID.
     *
     * @return returns the node if it is in nodes.
     */

    public Node getNode(int ID) throws NodeNotFoundException {
        Node output = nodes.get(ID);
        if (output != null) {
            return output;
        }
        throw new NodeNotFoundException(Integer.toString(ID));
    }
}
