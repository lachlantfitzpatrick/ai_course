package a1;

/**
 * Created by lachlan on 16/08/16.
 */
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class EnvironmentReader {

    /**
     * Reads an environment file and returns a graph corresponding to the file.
     */

    public static Graph readEnvironment(String environmentMapFileName) {
        Graph graph;
        try {
            File environmentFile = new File(environmentMapFileName);
            Scanner sc = new Scanner(environmentFile);
            int nodeID = 1;
            int vertexCount = sc.nextInt();
            graph = new Graph(vertexCount);

            while (sc.hasNext()) {
                Node node;
                int inGraph;
                try {
                    node = graph.getNode(nodeID);
                    inGraph = 1;
                } catch (NodeNotFoundException nnfe) {
                    node = new Node(nodeID);
                    inGraph = 0;
                }
                // Walk through the environment file. Adding node connections
                for (int i = 1; i <= vertexCount; i++) {
                    if (nodeID == i) {
                        String throwaway = sc.next(); // Consume scanner value
                    } else {
                        double cost = sc.nextDouble();
                        if (cost != 0.0) {
                            // Check if node in graph. If not, create it.
                            try {
                                node.addConnection(graph.getNode(i), cost);
                            } catch (NodeNotFoundException nnfe) {
                                Node newNode = new Node(i);
                                node.addConnection(newNode, cost);
                                graph.addNode(newNode);
                            }
                            if (inGraph == 0) {
                               graph.addNode(node);
                            }
                        }
                    }
                }
                nodeID++;
            }
            return graph;
        } catch (FileNotFoundException fnfe) {
            System.out.println(fnfe.getMessage());
            System.err.println("Error when loading file");
        }
        return new Graph(0);
    }
}
