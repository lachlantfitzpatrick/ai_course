package a1;

/**
 * An exception indicating the given node was not found in the collection.
 *
 * Created by lachlan on 22/08/16.
 */
public class NodeNotFoundException extends RuntimeException {

    public NodeNotFoundException() {
        super();
    }

    /**
     * Throws a runtime exception indicating that a node was not found in a
     * collection.
     * @param s The ID of the node that was not found.
     */
    public NodeNotFoundException(String s) {
        super(s);
    }

}
