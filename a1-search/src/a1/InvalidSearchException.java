package a1;

/**
 * An exception to indicate that there was a problem choosing the expand
 * algorithm.
 *
 * Created by lachlan on 22/08/16.
 */
public class InvalidSearchException extends RuntimeException {

    public InvalidSearchException() {
        super();
    }

    public InvalidSearchException(String s) {
        super(s);
    }
}
