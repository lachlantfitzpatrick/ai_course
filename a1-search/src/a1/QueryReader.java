package a1;

/**
 * Created by lachlan on 16/08/16.
 */
import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class QueryReader {

    /**
     * Reads a query file (.txt) and produces an array of queries
     */

    private String queryFileName;
    private Graph graph;
    /** Walks through a query file and stores the initialnode, goalnode and
     * requested expand algorithm in an array of Query data structures. This array is returned.
     */

    QueryReader(Graph graph, String queryFileName) {
        this.graph = graph;
        this.queryFileName = queryFileName;
    }

    public ArrayList<Query> readQueries(){
        Node initialNode, goalNode;
        int queryCount;
        File queryFile = new File(queryFileName);
        Scanner sc;
        Search searchAlgorithm = Search.UNIFORM_COST;
        ArrayList<Query> queryArray;

        try {
            sc = new Scanner(queryFile);

            queryCount = sc.nextInt();
            queryArray = new ArrayList<Query>(queryCount);

            while (sc.hasNext()) {
                String buffer = sc.next();
                if (buffer.equals("Uniform")) {
                    searchAlgorithm = Search.UNIFORM_COST;
                } else if (buffer.equals("A*")) {
                    searchAlgorithm = Search.A_STAR;
                }
                int initialNodeID = sc.nextInt();
                int goalNodeID = sc.nextInt();
                initialNode = graph.getNode(initialNodeID);
                goalNode = graph.getNode(goalNodeID);
                Query query = new Query(graph, initialNode, goalNode, searchAlgorithm);
                queryArray.add(query);
            }
            sc.close();
        } catch (FileNotFoundException fnfe) {
            queryArray = new ArrayList<Query>(0);
            System.out.println(fnfe.getMessage());
            System.err.println("Error in loading query file");
            System.exit(1);
        }
        return queryArray;

    }

}
