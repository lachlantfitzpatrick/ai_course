package a1;

import java.util.ArrayList;

/**
 * Created by lachlan on 16/08/16.
 */
public class main {

    /**
     * Runs the path optimisation program using the inputted files.
     *
     * @param args input files, the first string should be the environment map
     *             filename, the second should be the input query filename and
     *             the third be the output filename.
     */
    public static void main(String args[]){


        // Parse the input files.
        // Create an output file/object to write stuff to an output file.
        // Run the queries using the a1.QueryReader.QueryHandler.
        // Load environment
        EnvironmentReader er = new EnvironmentReader();
        Graph graph = er.readEnvironment(args[0]);

        // Load queries
        QueryReader qr = new QueryReader(graph, args[1]);

        // Run queryHandler
        long startTime = System.currentTimeMillis();
        QueryHandler qh = new QueryHandler();
        qh.runQueries(graph, qr.readQueries(), args[2]);
        long endTime = System.currentTimeMillis();
        long totalTime = endTime - startTime;

        System.out.println("\nTotal Time: " + totalTime + "ms");
    }

}
