package a1;

import java.util.LinkedList;

/**
 * The available expand algorithms.
 *
 * Created by lachlan on 22/08/16.
 */
public enum Search {
    UNIFORM_COST, A_STAR
}
