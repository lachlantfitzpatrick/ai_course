# COMP3702 Assignment 1

## Compilation Instructions

1. In the same directory as build.xml, run:

> ant

This will create compile and build the jar file from the source files.

2. Go into the directory where the jar file is contained:

> cd build/jar/

3. From here, you can run the jar file with environment files, query files and
output files using the following command:

> java -jar a1-3702-bunbois.jar environtMapFileName queryFileName outputFileName


Please contact, Hirotsugu Takahashi or Lachlan Fitzpatrick if you have any 
issues with the compilation or running of these files.
