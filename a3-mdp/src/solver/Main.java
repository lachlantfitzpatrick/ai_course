package solver;

import problem.ProblemSpec;
import problem.Simulator;
import solver.OfflineSolver;

import java.io.IOException;

/**
 * Created by swagahashi on 24/10/16.
 */
public class Main {

    /**
     * Given an Assignment 3 input file.
     * The program will conduct the transactions with the inventory manager
     * and output an appropriate output file.
     */
    public static void main(String[] args) {
        String problemSpecFile = "./test_case_generator/small-v1.txt";
        String outputFileName = "./test_case_generator/small-v3-sol.txt";
        try {
            Simulator simulator = new Simulator(problemSpecFile);
            simulator.setVerbose(false);
            ProblemSpec ps = new ProblemSpec(problemSpecFile);

            int tests = 1000;
            int success = 0, fail = 0;

            for(int i = 0; i < tests; i++) {
                OfflineSolver offlineSolver = new OfflineSolver(ps);

                /* Solve MDP using value iteration */
                offlineSolver.doOfflineComputation();

                /* Execute policies */
                for (int week = 0; week < ps.getNumWeeks(); week++) {
                    simulator.simulateStep(offlineSolver, week);
                }

                if(simulator.getTotalProfit() < 0) {
                    fail++;
                } else {
                    success++;
                }
                simulator.reset();
            }

            System.out.printf("\nSuccess Rate %d/%d\n", success, tests);

        } catch (IOException ioe) {
            System.out.println("Failed to read problemSpecFile!");
        }
    }
}
