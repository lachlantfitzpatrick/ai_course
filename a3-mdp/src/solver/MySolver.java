package solver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import problem.Store;
import problem.Matrix;
import problem.ProblemSpec;

public class MySolver implements OrderingAgent {

    private ProblemSpec spec = new ProblemSpec();
    private Store store;
    private List<Matrix> probabilities;

    private MCTS mcts;

    public static double discountFactor = 1.0;

    public MySolver(ProblemSpec spec) throws IOException {
        this.spec = spec;
        store = spec.getStore();
        probabilities = spec.getProbabilities();
        discountFactor = spec.getDiscountFactor();

        // Set the deadline.
        switch (store.getName()) {
            case "large":
            case "mega":
                Constants.DEADLINE = Constants.LONG_DEADLINE;
                break;
            default:
                Constants.DEADLINE = Constants.SHORT_DEADLINE;
        }

        mcts = new MCTS(spec);
    }

    public void doOfflineComputation() {
        // TODO Write your own code here.

    }

    public List<Integer> generateStockOrder(List<Integer> stockInventory,
                                            int numOfWeeksLeft) {
        return mcts.step(stockInventory, numOfWeeksLeft);
    }

}
