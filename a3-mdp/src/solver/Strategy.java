package solver;

/**
 * Created by lachlan on 30/10/16.
 */
public enum Strategy {
    RANDOM, EXPLORE, EXPLOIT
}
