package solver;

import problem.Matrix;
import problem.ProblemSpec;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by lachlan on 17/10/16.
 * <p>
 * A state that has been reached using an Action and will be proceeded from
 * using a Transition.
 */
public class TransitionState extends State {

    /**
     * The items that are currently stocked. Element i corresponds to the
     * number of items of type i that are in stock. The types must be indexed
     * from zero.
     */
    private List<Integer> stock;

    /**
     * The parent of this.
     */
    private Event parent;

    /**
     * The ProblemSpec of this.
     */
    private ProblemSpec ps;

    /**
     * The Transitions that are associated with this.
     */
    private HashMap<Transition, Transition> transitionHashMap = new HashMap<>();

    /**
     * Creates an instance of this with a stock.
     */
    protected TransitionState(ProblemSpec ps, List<Integer> stock,
                              Event parent) {
        super(ps, parent);
        this.ps = ps;
        this.stock = stock;
        this.parent = parent;
    }

    /**
     * Sample an Action and apply it to determine the resulting ActionState.
     */
    @Override
    public ActionState applySampledEvent(Strategy strategy) {
        Transition sample = generateSampledTransition();
        Transition mapValue = transitionHashMap.get(sample);
        if (mapValue == null) {
            return sample.getChild();
        } else {
            return mapValue.getChild();
        }
    }

    /**
     * Samples a random Transition.
     */
    private Transition generateSampledTransition() {
        List<Integer> order = initialiseList(stock.size(), 0);
        Random random = new Random();
        // Sample the order for each item.
        for (int i = 0; i < stock.size(); i++) {
            //double rand = random.nextDouble();
            List<Double> customerRow =
                    ps.getProbabilities().get(i).getRow(stock.get(i));
            // Sample the customer behaviour.
            double accumulatedProbability = 0;
            for (int numItems = 0; numItems < customerRow.size(); numItems++) {
                double rand = random.nextDouble();
                accumulatedProbability += customerRow.get(numItems);
                // We have selected a customer behaviour.
                if (rand < accumulatedProbability) {
                    order.set(i, numItems);
                    break;
                }
            }
        }
        return new Transition(this, order);
    }

    /**
     * Removes the Event from the data-structures if it exists.
     */
    @Override
    public void removeEvent(Event event) {
        // No need to do anything as we only have hashmap stuff.
    }

    /**
     * Updates the event e in the child data-structures and then returns the new
     * value of this.
     */
    @Override
    protected double updateEvents(Event e) {
        Transition t = (Transition) e;
        transitionHashMap.put(t, t);
        setValue(calculateValue());
        return getValue();
    }

    /*
     * Returns stock.
     */
    @Override
    protected List<Integer> getStock() {
        return new ArrayList<>(stock);
    }

    /**
     * Calculates the approximate value of the state, given what we know
     * about it
     */
    protected double calculateValue() {

        /* Get the value and probabilities of each transition */
        double expValue, weightedValue;
        double sumProb = 0, sumExpValue = 0;

        /* For every transition that we know of so far */
        for (Transition transition : transitionHashMap.values()) {
            /* Reset the transition probability */
            double prob = 1;

            /* Get the probability of that transition happening */
            for (int i = 0; i < ps.getStore().getMaxTypes(); i++) {
            /* For each item get the probability of a customer demanding what is being demanded*/
                prob *= ps.getProbabilities().get(i).get(stock.get(i),
                        transition.getDemand().get(i));
            }

            /* Accumulate the known probabilities */
            sumProb += prob;

            /* Get the expected value */
            expValue = prob * transition.getValue();

            /* Accumulate the expected values */
            sumExpValue += expValue;
        }

        /* Calculate the weighted average */
        weightedValue = sumExpValue/sumProb;

        return weightedValue;
    }


    /**
     * Calculates the value of this. For use in offline solution.
     */
    protected double calculateOfflineValue(HashMap<List<Integer>, ActionState>
                                                   stateSpace,
                                           List<List<Integer>> customerDemand) {
        /* Get the probabilities from ps */
        List<Double> priceList = ps.getPrices();
        List<Matrix> pMatrix = ps.getProbabilities();

        /* Get the expected sales revenue */
        double expectedValue = 0;

        /* For every scenario of customer demand */
        for (List<Integer> demand : customerDemand) {
            List<Integer> finalStock = new ArrayList<>(stock);

            /* Conduct a transaction */
            double transactionProfit = 0;
            double transactionProb = 1;
            /* For every item type the customer wants*/
            for (int i = 0; i < ps.getStore().getMaxTypes(); i++) {
                double itemProfit = 0.0;
                double itemProb = pMatrix.get(i).get(stock.get(i), demand.get(i));
                int stockSurplus = (stock.get(i) - demand.get(i));

                /* If store can meet the customer demand */
                if (stockSurplus >= 0) {
                     /* No missed opportunity */
                    itemProfit += (Constants.OUR_CUT * priceList.get(i) *
                            demand.get(i));
                    finalStock.set(i, stockSurplus);
                } else {
                    /* Missed opportunity */
                    itemProfit += (Constants.OUR_CUT * priceList.get(i) *
                            stock.get(i)
                            + stockSurplus * Constants.MISS *
                            priceList.get(i));
                    finalStock.set(i, 0);
                }
                transactionProfit += itemProfit;
                transactionProb *= itemProb;
            }
            double resultStateValue = stateSpace.get(finalStock).getValue();
            expectedValue += (transactionProb * (transactionProfit +
                    resultStateValue));

        }
        return ps.getDiscountFactor() * expectedValue;
    }

    /**
     * Conducts a weekly transaction given a stock and a demand and returns
     * the resulting profits
     * The profits are calculated from the sales made and factoring in losses
     * due to missed opportunities
     */

    /**
     * The Event that is used to proceed from a TransitionState to an
     * ActionState.
     */
    private class Transition extends Event {

        /**
         * List of purchases. These are all positive integers.
         */
        private List<Integer> demand;

        /**
         * The child of this.
         */
        private ActionState child;

        /**
         * The profit made by performing this transition.
         */
        private double profit = 0;

        /**
         * Constructor.
         */
        protected Transition(TransitionState parent, List<Integer> demand) {
            super(parent);
            this.demand = demand;
            setChild();
            setProfit(parent.getProblemSpec());
        }

        /**
         * Sets the profit gained by performing this transition. This depends
         * on the stock that is in parent.
         *
         * @param ps The ProblemSpec that supplies the prices.
         */
        @Override
        protected void setProfit(ProblemSpec ps) {
            profit = 0;
            // Number of items left after customer demand. Can be negative.
            int itemsLeft;
            for (int i = 0; i < ps.getStore().getMaxTypes(); i++) {
                itemsLeft = getParent().getStock().get(i) - demand.get(i);

                // Profit earned from sales. If there are more than or 0
                // items left then we only made sales (no missed
                // opportunities) however if there were 'less than zero'
                // items left then we made sales but also had missed
                // opportunities.
                profit = itemsLeft >= 0 ?
                        /* Only sales. */
                        profit + ps.getPrices().get(i) *
                                Constants.OUR_CUT * demand.get(i) :
                        /* Sales and missed opportunities. */
                        profit + getParent().getStock().get(i) *
                                ps.getPrices().get(i) * Constants.OUR_CUT +
                                itemsLeft * ps.getPrices().get(i) *
                                        Constants.MISS;
            }
        }

        /**
         * Returns the profit of this.
         */
        @Override
        protected double getProfit() {
            return this.profit;
        }

        /**
         * Generates and sets the child of this.
         */
        @Override
        protected void setChild() {
            this.child = generateChild();
        }

        /**
         * Generates and returns the child of this.
         */
        ActionState generateChild() {
            List<Integer> newStock = State.initialiseList(demand.size(), 0);
            // Number of items left after customer demand. Can be negative.
            int itemsLeft;
            // Generate the new stock remembering that the demand might
            // exceed the stock.
            for (int item = 0; item < demand.size(); item++) {
                itemsLeft = getParent().getStock().get(item) - demand.get(item);
                if (itemsLeft >= 0) {
                    newStock.set(item, getParent().getStock().get(item) -
                            demand.get(item));
                } else {
                    newStock.set(item, 0); // All items bought.
                }
            }
            return new ActionState(getProblemSpec(), newStock, this);
        }

        /**
         * Returns the child of this.
         */
        @Override
        protected ActionState getChild() {
            return this.child;
        }

        /**
         * Returns the demand of this.
         */
        protected List<Integer> getDemand() {
            return demand;
        }

        /**
         * The hashcode of this. This hashcode does not use value.
         */
        @Override
        public int hashCode() {
            int hash = 19;
            hash = hash * 65537 + demand.hashCode();
            return hash * 65537;
        }

        /**
         * Equals for this.
         */
        @Override
        public boolean equals(Object o) {
            if (!(o instanceof Transition)) {
                return false;
            }
            Transition t = (Transition) o;
            return t.hashCode() == this.hashCode();
        }
    }
}
