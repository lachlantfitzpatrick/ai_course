package solver;

import problem.ProblemSpec;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lachlan on 17/10/16.
 * <p>
 * Describes a state of a store.
 */
public abstract class State {

    /**
     * Initialises a List<Integer> to length n with value v as each element.
     */
    public static List<Integer> initialiseList(int n, int v) {
        ArrayList<Integer> out = new ArrayList<>(n - 1);
        for (int i = 0; i < n; i++) {
            out.add(v);
        }
        return out;
    }

    /**
     * The value of this. This will be used in value iteration
     */
    private Double value = 0.0;

    /**
     * Number of updates that have been performed on this.
     */
    private int updates = 0;

    /**
     * Whether this is the root.
     */
    private boolean isRoot = false;

    /**
     * The Event that moved us into this.
     */
    private Event parent = null;

    /**
     * The ProblemSpec of this.
     */
    private ProblemSpec ps = null;

    /**
     * Constructor. This is the only way to set parent.
     */
    protected State(ProblemSpec ps, Event parent) {
        this.parent = parent;
        this.ps = ps;
    }

    /**
     * Empty constructor.
     */
    protected State(ProblemSpec ps) {
        this.ps = ps;
    }

    /**
     * Gets ps.
     */
    protected ProblemSpec getProblemSpec() {
        return this.ps;
    }

    /**
     * Gets the value of this.
     */
    protected double getValue() {
        return value;
    }

    /**
     * Increments updates.
     */
    protected void update() {
        updates++;
    }

    /**
     * Gets the updates.
     */
    protected int getUpdates() {
        return updates;
    }

    /**
     * Sets the value of this based on the events.
     */
    protected void setValue(double value) {
        this.value = value;
    }

    /**
     * Sets this as root.
     */
    protected void setAsRoot() {
        isRoot = true;
    }

    /**
     * Gets whether this is root or not.
     */
    protected boolean isRoot() {
        return isRoot;
    }

    /**
     * Gets the parent.
     */
    protected Event getParent() {
        return parent;
    }

    /**
     * Back-propagates the value of this.
     */
    protected void backPropagate() {
        // Back-propagate if it is not the root.
        if (!isRoot) {
            parent.backPropagate(this);
        }
    }

    /**
     * Removes the action from the data-structures in this.
     */
    public abstract void removeEvent(Event e);

    /**
     * Back-propagate the value of this.
     */
    protected void backPropagate(Event child) {
        // Update the data-structures storing child and set the value.
        setValue(updateEvents(child));
        // Back-propagate the new value of this.
        if (!isRoot()) {
            parent.backPropagate(this);
        }
    }

    /**
     * Uses the event e to update the appropriate data-structure then gets
     * the new value of this.
     */
    protected abstract double updateEvents(Event e);

    /**
     * Gets the stock of this.
     */
    protected abstract List<Integer> getStock();

    /**
     * Samples for a State that is a child of this.
     */
    protected abstract State applySampledEvent(Strategy strategy);

    /**
     * Hashcode for this. This hashcode does not use value.
     */
    @Override
    public int hashCode() {
        int hash = 19;
        hash = hash * 65537 + getStock().hashCode();
        return hash;
    }

    /**
     * Equals for this.
     */
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof State)) {
            return false;
        }
        State s = (State) o;
        return this.hashCode() == s.hashCode();
    }
}
