package solver;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lachlan on 18/10/16.
 */
public class MiscTests {


    @Test
    public void testInstanceOf() {
        Integer n = null;
        Assert.assertFalse(n instanceof Integer);
    }

    @Test
    public void removingIntegerFromIntegerList() {
        List<Integer> intList = new ArrayList<>();
        intList.add(new Integer(1));
        intList.add(1);
        intList.remove(new Integer(1));
        intList.remove(new Integer(1));
        Assert.assertTrue(intList.size() == 0);
    }

    @Test
    public void arithmeticSeriesProbability() {
        int size = 123;
        double weighting = (double) size * (size + 1) / 2;
        double cumulativeProbability = 0;
        for (int i = 0; i < size; i++) {
            cumulativeProbability += (double) 2 * (size - i) / weighting;
            System.out.println(cumulativeProbability);
        }
    }
}
