package solver;

/**
 * Created by lachlan on 18/10/16.
 *
 * A series of constant parameters that are used to tune the algorithm.
 */
public class Constants {

    /** Determines the depth a simulation goes to. */
    public static final int MAX_DEPTH = 20;

    /** The amount of time to run a step for in milliseconds. */
    public static long DEADLINE = 5000;
    /** The amount of time to run a tiny, small or medium store for. */
    public static final long SHORT_DEADLINE = 30000;
    /** The amount of time to run a tiny, small or medium store for. */
    public static final long LONG_DEADLINE = 5000;

    /** The safety factor to apply to the longest time of a simulation. */
    public static final long SAFETY_FACTOR = 5;

    /** Price multiplier for profit */
    public static final double OUR_CUT = 0.75;

    /** Price multiplier for missing a sale. */
    public static final double MISS = 0.25;

    /** Price multiplier for return. */
    public static final double RETURN_FACTOR = 0.5;

    /** Probability of applying exploit strategy. */
    public static final double EXPLOIT = 0.01;

    /** Probability of applying the explore strategy. */
    public static final double EXPLORE = 0.50;

}
