package solver;

import problem.ProblemSpec;
import problem.Store;

import java.util.List;
import java.util.Random;

/**
 * Created by lachlan on 17/10/16.
 * <p>
 * Performs a Monte-Carlo Tree Search.
 */
public class MCTS {

    /**
     * The store this is running on.
     */
    private Store store;

    /**
     * The problem spec this is running on.
     */
    private ProblemSpec ps;

    /**
     * The maxDepth of the simulation.
     */
    private int maxDepth = Constants.MAX_DEPTH;

    /**
     * The time that the last run took to converge in milliseconds.
     */
    private long convergeTime;

    /**
     * Creates an instance of this using ps.
     */
    public MCTS(ProblemSpec ps) {
        this.ps = ps;
        this.store = ps.getStore();
    }

    /**
     * Runs a Monte-Carlo Tree Search on ps and returns order that has been
     * selected as the next move.
     */
    public List<Integer> step(List<Integer> stock, int numWeeksLeft) {

        if (numWeeksLeft < Constants.MAX_DEPTH) {
            maxDepth = numWeeksLeft;
        }

        // Create the root state.
        ActionState root = new ActionState(ps, stock);

        long time = 0; // Time since we initiated the search.
        long initiate = System.currentTimeMillis();
        long start;
        long longestTime = 0;
        List<Integer> bestOrder = ActionState.initialiseList(stock.size(), 0);
        while (time < Constants.DEADLINE - longestTime * Constants
                .SAFETY_FACTOR) {

            start = System.currentTimeMillis() - initiate;

            // 1. Select a path... use epsilon-greedy.
            ActionState fringe = getFringe(root);
            // 3. Simulate from the node that was expanded.
            // Select an action using epsilon-greedy and simulate from there.
            simulate(fringe);
            // 4. Back-propagate the results.
            fringe.backPropagate();

            root.update();

//            if(!root.checkInvariant() || !fringe.checkInvariant()) {
//                System.out.println();
//            }

            time = System.currentTimeMillis() - initiate;
            if (time - start > longestTime) {
                longestTime = time - start;
            }

            if(!root.getBestOrder().equals(bestOrder)) {
                bestOrder = root.getBestOrder();
                convergeTime = time;
            }
        }
        // Testing code.
//        System.out.println();
//        System.out.println();
//        System.out.println("Times updated: " + root.getUpdates());
//        System.out.println("Time to complete: " + (System.currentTimeMillis()
//                - initiate));
//        root.checkInvariant();
//        // See if the number of updates that were recorded on root is the
//        // same as the number of updates recorded on the children of root.
//        int rootUpdates = root.getUpdates();
//        int rootChildrenUpdates = root.getChildrenUpdates();
//        if(rootUpdates == rootChildrenUpdates) {
//            System.out.println("YAY");
//        }
        System.out.println("Converge time: " + getConvergeTime());

        return root.getBestOrder();
    }

    /**
     * Finds the next state that will be simulated using current as the
     * start of the search.
     */
    private ActionState getFringe(ActionState current) {
        // Select the strategy using multi-arm bandit.
        Strategy strategy;
        double random = new Random().nextDouble();
        if (random < Constants.EXPLOIT) {
            strategy = Strategy.EXPLOIT;
        } else if (random < Constants.EXPLORE) {
            strategy = Strategy.EXPLORE;
        } else {
            strategy = Strategy.RANDOM;
        }
        // Keep searching down the tree until we find an ActionState that
        // hasn't been expanded. Expand it and then return it.
        while (current.isExpanded()) {
            current = sampleNext(current, strategy);
        }
        current.expand();
        return current;
    }

    /**
     * Takes current and samples down to the next current.
     */
    private ActionState sampleNext(ActionState current, Strategy strategy) {
        // Sample an Action to move to a TransitionState.
        TransitionState ts = current.applySampledEvent(strategy);
        // Sample a Transition to move to an ActionState.
        return ts.applySampledEvent(strategy);
    }

    /**
     * Simulates the behaviour of the store for a number of steps until
     * maxDepth.
     *
     * @require initial must have been initialised to be simulated.
     */
    private void simulate(ActionState initial) {
        initial.setValue(simulate(initial, 0));
    }

    /**
     * Simulates the behaviour of the store at maxDepth.
     *
     * @return the expected profit.
     */
    private double simulate(ActionState current, int currentDepth) {
        if (currentDepth < this.maxDepth) {
            // Sample an Action to move to a TransitionState.
            TransitionState nextTransitionState =
                    current.applySampledEvent(Strategy.RANDOM);
            // Sample a Transition to move to an ActionState.
            ActionState nextActionState =
                    nextTransitionState.applySampledEvent(Strategy.RANDOM);

            // Simulate from the ActionState sampled above to get the
            // expected profit from future steps.
            double horizonProfit = simulate(nextActionState, currentDepth + 1);
            // Get the profit from this step.
            double stepProfit = nextActionState.getParent().getProfit() +
                    nextTransitionState.getParent().getProfit();

            // Bellman backup.
            return MySolver.discountFactor * horizonProfit + stepProfit;

        } else {
            // Return 0 to indicate that we don't know what the future profit
            // will be but we have finished our simulation.
            return 0;
        }
    }

    /**
     * The the time it has taken to converge.
     */
    public long getConvergeTime() {
        return convergeTime;
    }
}
