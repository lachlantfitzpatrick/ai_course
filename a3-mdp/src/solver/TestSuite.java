package solver;

import org.junit.Test;

/**
 * Created by lachlan on 26/10/16.
 *
 * Runs all of the tests that should pass to ensure that the code is functional!
 */
public class TestSuite {

    /**
     * Test all of the other tests.
     */
    @Test
    public void testSuite() {

        // Test the State class.
        new StateTests().testActionSpaceGeneration();
        new StateTests().testRandomActionGeneration();

        // Test the ActionState class.
        new ActionStateTest().testCalculateValue();
        new ActionStateTest().testGetBestOrder();
        new ActionStateTest().testSetBestAction();
        new ActionStateTest().testStateSpaceUpdate();
        new ActionStateTest().testApplySampledEvent();

        // Test the offline solver.
        new OfflineSolverTest().testGenerateCustomerDemand();
        new OfflineSolverTest().testStateSpaceGeneration();
        new OfflineSolverTest().testValueIteration();
    }
}
