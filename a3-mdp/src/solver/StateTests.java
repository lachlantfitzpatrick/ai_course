package solver;

import org.junit.Assert;
import org.junit.Test;
import problem.ProblemSpec;
import problem.Store;
import solver.ActionState;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lachlan on 16/10/16.
 */
public class StateTests {

    /**
     * Tests the generation of the action space.
     * <p>
     * It was very difficult to automate the test of the action generation so
     * this class was used primarily for running the class such that it could
     * be tested through the debugger.
     */
    @Test
    public void testActionSpaceGeneration() {
        try {

            ProblemSpec ps = new ProblemSpec("./test_case_generator/test_cases/large store test cases/large-1" +
                    ".txt");
            List<Integer> stock = new ArrayList<>();
            stock.add(1);
            stock.add(1);
            stock.add(1);
            stock.add(1);
            stock.add(1);
//        stock.add(3);
//        stock.add(3);

            long start = System.currentTimeMillis();
            ActionState actionState = new ActionState(ps, stock);
            actionState.generateAllActions();
            System.out.println(System.currentTimeMillis() - start);

            Assert.assertTrue(actionState.checkInvariant());
        } catch (IOException e) {
            System.out.println("We couldn't read the file in.");
            Assert.assertTrue(false);
        }
    }

    /**
     * Tests the random actionGeneration. Relies on the total actionSpace
     * being generated.
     */
    @Test
    public void testRandomActionGeneration() {
        try {
            // IMPORTANT: this can only test up to a small store due to the
            // stock size.
            ProblemSpec ps = new ProblemSpec("./testcases/small-v1.txt");
            // Make a small store.
            Store store = ps.getStore();
            for (int j = 0; j < store.getCapacity(); j++) {
                for (int k = 0; k < store.getCapacity() - j; k++) {
                    List<Integer> stock = new ArrayList<>();
                    stock.add(j);
                    stock.add(k);

                    ActionState as1 = new ActionState(ps, stock);
                    as1.generateAllActions();

                    ActionState as2 = new ActionState(ps, stock);

                    for (int i = 0; i < 100000; i++) {
                        Event action = as2.generateAndAddSampledAction();
                        if (as1.containsAction(action)) {
                            Assert.assertTrue(true);
                        } else {
                            Assert.assertTrue(false);
                        }
                    }

                    Assert.assertTrue(as1.checkInvariant());
                    Assert.assertTrue(as2.checkInvariant());

                    // Check all actions were generated while sampling.
                    Assert.assertTrue(as1.checkActionSpaceSpan(as2));
                }
            }
        } catch (IOException ex) {
            System.out.println("Couldn't read file in.");
            Assert.assertTrue(false);
        }
    }
}
