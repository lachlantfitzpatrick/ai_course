package solver;

import org.junit.Assert;
import org.junit.Test;
import problem.ProblemSpec;
import problem.Store;

import java.io.IOException;
import java.util.*;
import java.util.List;

/**
 * Created by swagahashi on 21/10/16.
 */
public class ActionStateTest {


    @Test
    public void testSetBestAction() {
        String problemSpecFile = "./testcases/ex1-tiny.txt";

        try {
            /* Initialise problem */
            ProblemSpec ps = new ProblemSpec(problemSpecFile);
            OfflineSolver offlineSolver = new OfflineSolver(ps);

            List<Integer> stock = new ArrayList<>();
            offlineSolver.generateStateSpace(stock, 0, -1);
            List<Integer> demand = new ArrayList<>();
            offlineSolver.generateCustomerDemand(demand, 0);

            ActionState rootAS = offlineSolver.getStateSpace().get(ps
                    .getInitialStock());
            rootAS.generateAllActions();

            ArrayList<Double> valueList = new ArrayList<>();

            /* Iterate through all actions and calculate values of children */
            ArrayList<TransitionState> transitionStates = rootAS
                    .getTransitionStates();

            for (TransitionState transState : transitionStates) {
                transState.setValue(transState.calculateOfflineValue(offlineSolver
                                .getStateSpace(),
                        offlineSolver.getCustomerDemand()));
                double value = transState.getValue();
                double profit = transState.getParent().getProfit();
                valueList.add(value + profit);
                System.out.println(transState.getValue());
            }

            Collections.sort(valueList, Collections.reverseOrder());

            /* Regenerate StateSpace */
            offlineSolver.generateStateSpace(stock, 0, -1);

            /* Calculate the best action */
            ActionState rootAS2 = offlineSolver.getStateSpace().get(ps
                    .getInitialStock());
            rootAS2.generateAllActions();
            rootAS2.setBestAction(offlineSolver.getStateSpace(),
                    offlineSolver.getCustomerDemand());

            System.out.println();
            System.out.println(valueList.get(0));
            System.out.println(rootAS2.getValue());

            /* Check that the bestChild values are consistent */
            Assert.assertTrue(valueList.get(0) == rootAS2.getValue());

        } catch (IOException ioe) {
            Assert.assertTrue(false);

        }
    }

    @Test
    public void testCalculateValue() {
        String problemSpecFile = "./testcases/ex1-tiny.txt";

        try {
            ProblemSpec ps = new ProblemSpec(problemSpecFile);
            OfflineSolver offlineSolver = new OfflineSolver(ps);

            List<Integer> stock = new ArrayList<>();
            offlineSolver.generateStateSpace(stock, 0, -1);
            List<Integer> demand = new ArrayList<>();
            offlineSolver.generateCustomerDemand(demand, 0);

            HashMap<List<Integer>, ActionState> currentSS = offlineSolver
                    .getStateSpace();

            double currentVal, newVal, delta = 0.0;
            int count = 1;
            do {
                System.out.printf("Loop %3d | ", count);
                delta = 0.0;
                for (ActionState actionState : offlineSolver.getStateSpace()
                        .values()) {
                    currentVal = actionState.getValue();
                    actionState.setBestAction(currentSS, offlineSolver
                            .getCustomerDemand());
                    newVal = actionState.getValue();
                    if (Math.abs(currentVal - newVal) > delta) {
                        delta = Math.abs(currentVal - newVal);
                    }
                }
                System.out.printf("delta = %f\n", delta);
                currentSS = offlineSolver.getStateSpace();
                count++;
            } while (delta > 0.0);

        } catch (IOException ioe) {
            Assert.assertTrue(false);
        }

    }

    @Test
    public void testGetBestOrder() {
        String problemSpecFile = "./testcases/ex1-tiny.txt";

        try {
            ProblemSpec ps = new ProblemSpec(problemSpecFile);
            OfflineSolver offlineSolver = new OfflineSolver(ps);

            List<Integer> stock = new ArrayList<>();
            offlineSolver.generateStateSpace(stock, 0, -1);
            List<Integer> demand = new ArrayList<>();
            offlineSolver.generateCustomerDemand(demand, 0);


        } catch (IOException ioe) {
            Assert.assertTrue(false);

        }
    }

    @Test
    public void testStateSpaceUpdate() {
        String problemSpecFile = "./testcases/small-v1.txt";

        try {
            ProblemSpec ps = new ProblemSpec(problemSpecFile);
            OfflineSolver offlineSolver = new OfflineSolver(ps);

            List<Integer> stock = new ArrayList<>();
            offlineSolver.generateStateSpace(stock, 0, -1);
            /* Copy of the initial state space | state space at loop = 0 */
            HashMap<List<Integer>, ActionState> currentStateSpace =
                    offlineSolver.getStateSpace();

            List<Integer> demand = new ArrayList<>();
            offlineSolver.generateCustomerDemand(demand, 0);

            //ActionState rootAS = new ActionState(ps, ps.getInitialStock());
            //rootAS.generateAllActions();

            int loops = 3;
            double currentVal, newVal, delta;

            for (int i = 0; i < loops; i++) {
                delta = 0.0;
                System.out.printf("Loop %d\n", i + 1);
                for (ActionState actionState : offlineSolver.getStateSpace()
                        .values()) {
                    currentVal = actionState.getValue();
                    actionState.setBestAction(currentStateSpace,
                            offlineSolver.getCustomerDemand());
                    newVal = actionState.getValue();
                    if (Math.abs(newVal - currentVal) > delta) {
                        delta = Math.abs(newVal - currentVal);
                    }
                }
                System.out.println(delta);
                /* Update the stateSpace lookUp reference */
                currentStateSpace = offlineSolver.getStateSpace();
            }

        } catch (IOException ioe) {
            Assert.assertTrue(false);

        }
    }

    /**
     * Tests applying a sampled event.
     */
    @Test
    public void testApplySampledEvent() {
        try {
            ProblemSpec ps = new ProblemSpec("./testcases/small-v1.txt");
            // Make a small store.
            Store store = ps.getStore();
            for (int j = 0; j < store.getCapacity(); j++) {
                for (int k = 0; k < store.getCapacity() - j; k++) {
                    List<Integer> stock = new ArrayList<>();
                    stock.add(j);
                    stock.add(k);

                    ActionState actionState = new ActionState(ps, stock);

                    for (int i = 0; i < 100000; i++) {
                        // Get a sampled event.
                        TransitionState ts = actionState.applySampledEvent(Strategy.RANDOM);
                        ts.setValue(10.0);
                        ts.backPropagate();
                        actionState.updateEvents(ts.getParent());
                        // Check that the parent of ts is an Action that is
                        // contained in actionState.
                        Assert.assertTrue(actionState.containsAction(
                                ts.getParent()));

                        // Check that applying the action to the ActionState
                        // gets the transition state.
                        Assert.assertTrue(
                                checkAction(ts.getParent().getDemand(),
                                        actionState.getStock(),
                                        ts.getStock()));

                    }
                }
            }
        } catch (IOException e) {
            System.out.println("Failed reading file.");
            Assert.assertTrue(false);
        }
    }

    /**
     * Helper method to check that applying the given action to a given stock
     * results in the given resulting stock.
     */
    private boolean checkAction(List<Integer> action, List<Integer> initial,
                                List<Integer> output) {
        if (action.size() != initial.size() || action.size() != output.size()) {
            return false;
        }
        for (int i = 0; i < action.size(); i++) {
            if (initial.get(i) + action.get(i) != output.get(i)) {
                return false;
            }
        }
        return true;
    }
}
