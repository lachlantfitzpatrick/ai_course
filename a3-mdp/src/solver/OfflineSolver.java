package solver;

import problem.ProblemSpec;
import problem.Store;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Created by swagahashi on 17/10/16.
 * This class is responsible for solving Markov Decision Processes using
 * offline solutions, namely, value iteration
 */

public class OfflineSolver implements OrderingAgent {

    private List<List<Integer>> customerDemands = new ArrayList<>();
    private HashMap<List<Integer>, ActionState> stateSpace = new HashMap<>();

    private Store store;
    private ProblemSpec ps;

    public class STOCK_REPR implements Comparator<List<Integer>>{

        @Override
        public int compare(List<Integer> stock1, List<Integer> stock2){

            for(int i = 0; i < stock1.size(); i++) {
                if(stock1.get(i) < stock2.get(i)) {
                    return -1;
                } else if (stock1.get(i) > stock2.get(i)) {
                    return 1;
                }
            }
            return 0;
        }
    }

    private class STATE_REPR implements Comparator<ActionState>{

        @Override
        public int compare(ActionState aS1, ActionState aS2){

            for(int i = 0; i < aS1.getStock().size(); i++) {
                if(aS1.getStock().get(i) < aS2.getStock().get(i)) {
                    return -1;
                } else if (aS1.getStock().get(i) > aS2.getStock().get(i)) {
                    return 1;
                }
            }
            return 0;
        }
    }

    /**
     * Constructs the OfflineSolver object and initialises object parameters
     */
    public OfflineSolver(ProblemSpec ps){
        this.ps = ps;
        this.store = ps.getStore();
    }

    public void doOfflineComputation() {
        valueIteration();
    }

    /**
     * Generates the optimal policy using the Bellman Update
     * Keep updating the utility values of each
     * state until the value has reached an optimal state or until 180 seconds elapses
     */
    public void valueIteration() {
        System.out.println("Executing value iteration");
        double tick = System.currentTimeMillis();

        /* Generate stateSpace */
        List<Integer> emptyStock = new ArrayList<>();
        generateStateSpace(emptyStock, emptyStock.size(), -1);

        /* Copy of the initial state space | state space at loop = 0 */
        HashMap<List<Integer>, ActionState> currentStateSpace = new HashMap<>(getStateSpace());

        /* Generate customerDemands */
        List<Integer> emptyDemand = new ArrayList<>();
        generateCustomerDemand(emptyDemand, 0);

        /* Value iteration loop */

        int count = 1;
        double currentVal, newVal, delta;
        do {
            delta = 0.0;
            /* For each state run a value update */
            for (ActionState actionState : stateSpace.values()) {
                currentVal = actionState.getValue();
                actionState.setBestAction(currentStateSpace, customerDemands);
                newVal = actionState.getValue();
                if(Math.abs(newVal - currentVal) > delta) {
                    delta = Math.abs(newVal - currentVal);
                }
                stateSpace.put(actionState.getStock(), actionState);
            }
            /* Update the value of state in the stateSpace */
            //System.out.printf("Loop %4d | Delta: '%5.3e'\n", count, delta);
            currentStateSpace = new HashMap<>(getStateSpace());
            count++;
        } while (delta > 0.1);
        System.out.println("Done!");
        System.out.printf("Calculation time %fms\n", (System.currentTimeMillis() - tick));
    }

    /**
     * Returns a list of actionStates sorted according to their stock arrangement
     */
    public List<ActionState> getSortedStateSpace() {
        List<ActionState> sortedStateSpace = new ArrayList<>();

        for(ActionState actionState : stateSpace.values()) {
            sortedStateSpace.add(actionState);
        }

        Collections.sort(sortedStateSpace, new STATE_REPR());

        return sortedStateSpace;
    }

    public void printPolicy() {
        List<List<Integer>> stateList = new ArrayList<>();
        List<List<Integer>> offlineList = new ArrayList<>();

        for(ActionState actionState : stateSpace.values()) {
            stateList.add(actionState.getStock());
        }

        Collections.sort(stateList, new STOCK_REPR());

        System.out.printf("\nPrinting Policy: \n");

        for(List<Integer> stock : stateList) {
            System.out.print("State " + Arrays.toString(stock.toArray()));
            System.out.print(" | ");
            System.out.print("Policy " + Arrays.toString(stateSpace.get(stock).getBestOrder().toArray()));
            System.out.println();
        }
    }

    public List<Integer> generateStockOrder(List<Integer> afterConsumptionInventory,
                                   int numWeeksLeft) {
        return stateSpace.get(afterConsumptionInventory).offlineGetBestOrder();
    }

    /**
     * Generates all stock states
     * Note to self;
     * With recursive algorithms, you need to pass in all the necessary
     * parameters as arguments, especially when you're going to limit your
     * problem space by reducing the cap in a for loop.
     */
    public void generateStateSpace(List<Integer> stock,
                                  int currentStockSize, int startIndex) {

        /* When stockSize has reached maxCapacity of the store, we know that
         we have filled up the stockstate and are ready to process! */
        if(currentStockSize >= store.getCapacity()) {
            /* Process the completed stock state appropriately */
            List<Integer> stockState = new ArrayList<>(processStockState(stock));
            //for (Integer i : stockState) {
            //    System.out.printf("%2d ", i);
            //}
            //System.out.println();
            ActionState actionState = new ActionState(this.ps, stockState);
            this.stateSpace.put(stockState, actionState);
            return;
        }

        /* Let the itemIndex loop from startIndex to maxItemindex */
        for(int currentIndex = startIndex; currentIndex < store.getMaxTypes(); currentIndex++) {
            List<Integer> next = new ArrayList<>(stock);
            next.add(currentIndex);
            generateStateSpace(next, currentStockSize + 1, currentIndex);
        }
    }

    /**
     * Initialises a List<Integer> to length n with value v as each element.
     */
    public List<Integer> initialiseList(int n, int v) {
        ArrayList<Integer> out = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            out.add(v);
        }
        return out;
    }

    /**
     * Let's say we're given a stock that has a maxCapacity of 7 items and
     * store 4 types of items. This function processes the stocks in the form:
     * {2, 3, 0, 2, 3, 3, -1} into {1,0,2,3}
     * Note that the items are indexed from 0. So item 1 is represented by 0.
     * The example above shows an inventory of:
     * Two item3s, three item4s and one item1s, totalling 6 items.
     */
    private List<Integer> processStockState(List<Integer> stock) {
        List<Integer> stockState = initialiseList(store.getMaxTypes(), 0);
        for(int i = 0; i < stock.size(); i ++) {
            if(stock.get(i) != -1) {
                stockState.set(stock.get(i), stockState.get(stock.get(i)) + 1);
            }
        }
        return stockState;
    }

    public void generateCustomerDemand(List<Integer> demandList, int currentDemand) {
        /* Note to self: If you ever want to do a n-depth for loop. Just do recursion */
        /* Note to Yuji: that seems like a good note to self. */
        if(currentDemand >= store.getMaxTypes()) {
            this.customerDemands.add(demandList);
            return;
        }

        for(int itemIndex = 0; itemIndex <= store.getCapacity(); itemIndex++) {
            List<Integer> next = new ArrayList<>(demandList);
            next.add(itemIndex);
            generateCustomerDemand(next, currentDemand + 1);
        }

    }

    /**
     * Returns the possible demands of customers
     */
    public List<List<Integer>> getCustomerDemand() {
        return this.customerDemands;
    }

    /**
     * Returns the stateSpace of the offlineSolver
     */
    public HashMap<List<Integer>, ActionState> getStateSpace() {
        return this.stateSpace;
    }
}

