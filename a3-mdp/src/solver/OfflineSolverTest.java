package solver;

import org.junit.Assert;
import org.junit.Test;
import problem.ProblemSpec;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by swagahashi on 18/10/16.
 */
public class OfflineSolverTest {

    /**
     *  Question for self:
     *  Should classes be as modular as possible?
     *      Yeah probably.
     *      Try to design your classes so that they can be
     *      tested without dependencies on other classes
     */

    /**
     * Tests the generation of the state space.
     * The sum of the inventories should never
     * exceed the maximum capacity of the store
     */
    @Test
    public void testStateSpaceGeneration() {
        String problemSpecFile = "./testcases/ex1-tiny.txt";
        try {
            ProblemSpec ps = new ProblemSpec(problemSpecFile);
            OfflineSolver offlineSolver = new OfflineSolver(ps);

            /* Initial stock state */
            List<Integer> stock = new ArrayList<>();

            /* Generate all possible stock states from initial */
            offlineSolver.generateStateSpace(stock, 0, -1);
            int count = 0;
            for (ActionState actionState : offlineSolver.getStateSpace().values()) {
                int sum = 0;
                for (int i = 0; i < actionState.getStock().size(); i++) {
                    sum = sum + actionState.getStock().get(i);
                }
            /* Check that the capacity of the store is not being exceeded */
                Assert.assertTrue(sum <= ps.getStore().getCapacity());
                count++;
            }
            System.out.printf("Storetype: '%s'\nStates generated: %d\n",
                    ps.getStore().getName(), count);

            /* Check if we can retrieve from the state space using stock */
            Assert.assertTrue(offlineSolver.getStateSpace().get(ps.getInitialStock()) != null);
            ArrayList<Integer> badStock = (ArrayList<Integer>) ps.getInitialStock();
            badStock.set(0, 10000);
            Assert.assertTrue(offlineSolver.getStateSpace().get(badStock) == null);


        } catch (IOException ioe) {
            Assert.assertTrue(false);

        }
    }

    @Test
    public void testGenerateCustomerDemand() {
        String problemSpecFile = "./testcases/small-v1.txt";
        try {
            ProblemSpec ps = new ProblemSpec(problemSpecFile);
            OfflineSolver offlineSolver = new OfflineSolver(ps);

            List<Integer> emptyDemand = new ArrayList<>();

            offlineSolver.generateCustomerDemand(emptyDemand, 0);

            List<List<Integer>> customerDemand = offlineSolver.getCustomerDemand();

            int count = 0;
            for(List<Integer> demand : customerDemand) {
                for(int i = 0; i < demand.size(); i++) {
                    System.out.print(demand.get(i) + " ");
                }
                count++;
                System.out.println();
            }
            System.out.printf("\ncount %d\n", count);

        } catch(IOException ioe) {
            Assert.assertTrue(false);
        }
    }

    @Test
    public void testValueIteration(){
        String problemSpecFile = "./testcases/ex1-tiny.txt";
        try {
            ProblemSpec ps = new ProblemSpec(problemSpecFile);
            OfflineSolver offlineSolver = new OfflineSolver(ps);
            offlineSolver.valueIteration();
            offlineSolver.printPolicy();
        } catch(IOException ioe) {
            Assert.assertTrue(false);
        }
    }
}