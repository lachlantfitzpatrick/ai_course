package solver;

import problem.ProblemSpec;
import problem.Store;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by lachlan on 16/10/16.
 * <p>
 * A state that has been reached using a Transition or is the root and will
 * be proceeded from using an Action.
 */
public class ActionState extends State {

    /**
     * Value for an empty type. Eg. when not returning anything.
     */
    private static final int EMPTY_TYPE = -1;

    /**
     * The items that are currently stocked. Element i corresponds to the
     * number of items of type i that are in stock. The types must be indexed
     * from zero.
     */
    private List<Integer> stock;

    /**
     * Actions that can be performed from this.
     */
    private TreeSet<Action> actionsValueSet =
            new TreeSet<>(new ActionValueComparator());
    private TreeSet<Action> actionsUpdateSet =
            new TreeSet<>(new ActionUpdateComparator());
    private Action bestAction = null;

    // Maps an Action to the most recently updated version of itself.
    private HashMap<Action, Action> actionsHashMap = new HashMap<>();

    /**
     * The ProblemSpec and Store associated with this. store is the store of ps.
     */
    private Store store;

    private int stateUpdate = 0;

    /**
     * Indicates whether this has been expanded.
     */
    private boolean expanded = false;

    /**
     * Creates an instance of this and generates the actions.
     */
    protected ActionState(ProblemSpec ps, List<Integer> stock, Event parent) {
        super(ps, parent);
        this.store = ps.getStore();
        this.stock = stock;
    }

    /**
     * Creates a root instance of this and generates the actions.
     */
    protected ActionState(ProblemSpec ps, List<Integer> stock) {
        super(ps);
        this.store = ps.getStore();
        this.stock = stock;
        setAsRoot();
        expanded = true;
    }

    /**
     * Expands this. This requires the current actions to be reset.
     */
    protected void expand() {
        expanded = true;
    }

    /**
     * Gets whether this is expanded.
     */
    protected boolean isExpanded() {
        return expanded;
    }

    /**
     * Generates a single action randomly and adds it to this before
     * returning it.
     */
    protected Action generateAndAddSampledAction() {
        Action a = generateSampledAction();
        removeEvent(a);
        addAction(a);
        return a;
    }

    /**
     * Generates a single action randomly.
     */
    protected Action generateSampledAction() {
        // The number of 'orderActions' that are available. This is the sum
        // of the number of returns and number of purchases able to be made.
        int maxPurchases = store.getMaxPurchase();
        int numPurchases = 0;
        List<Integer> purchases = initialiseList(stock.size(), 0);
        int maxReturns = store.getMaxReturns();
        int numReturns = 0;
        List<Integer> returns = initialiseList(stock.size(), 0);
        int numOrderActions = maxPurchases + maxReturns;

        Random random = new Random();

        // Determine whether we want to perform another order action or not.
        while (!(random.nextDouble() < (double) 1 / (numOrderActions + 1))) {
            // Determine whether we want to perform a return or purchase.
            if (random.nextDouble() <
                    (double) maxReturns / (maxPurchases + maxReturns)) {
                if (numReturns < maxReturns) {
                    // Do a return.
                    returns = sampleReturn(purchases, returns);
                    numReturns++;
                } else {
                    // Hack to try and reduce the high number of empty orders.
                    numOrderActions++;
                }
            } else {
                if (numPurchases < maxPurchases) {
                    // Do purchase.
                    purchases = samplePurchase(purchases, returns);
                    numPurchases++;
                } else {
                    // Hack to try and reduce the high number of empty orders.
                    numOrderActions++;
                }
            }
            if (numPurchases + numReturns == maxPurchases + maxReturns) {
                break;
            }
        }
        Action newAction = new Action(this, purchases, returns);
        Action mapValue = actionsHashMap.get(newAction);
        if (mapValue == null) {
            return newAction;
        } else {
            removeEvent(mapValue);
            // Testing code to check the duplicates occurring in the
            // data-structures.
//            // If sizes are not equal in the data structures lets investigate!
//            if (actionsHashMap.size() != actionsValueSet.size() ||
//                    actionsUpdateSet.size() != actionsHashMap.size()) {
//                ActionUpdateComparator AUC = new ActionUpdateComparator();
//                // Find the elements that are different.
//                if (actionsUpdateSet.size() > actionsValueSet.size()) {
//                    // Check duplicates.
//                    for (Action a1 : actionsUpdateSet) {
//                        boolean isFound = false;
//                        for (Action a2 : actionsUpdateSet) {
//                            if (a1.equals(a2)) {
//                                if (isFound) {
//                                    // Duplicate.
//                                    int i = AUC.compare(a1, a2);
//                                    System.out.println(i);
//                                }
//                                isFound = true;
//                            }
//                        }
//                    }
//                    // Check for elements that one contains but the other
//                    // doesn't.
//                    for (Action a1 : actionsUpdateSet) {
//                        boolean isFound = false;
//                        for (Action a2 : actionsValueSet) {
//                            if (a1.equals(a2)) {
//                                isFound = true;
//                            }
//                        }
//                        if (!isFound) {
//                            // Update set has more elements that action set.
//                            // Means that we didn't remove newAction.
//                            int i = AUC.compare(a1, newAction);
//                            System.out.println(i);
//                        }
//                    }
//                }
//            }
            return mapValue;
        }
    }

    /**
     * Samples an allowed purchase based on the order, stock and number of
     * purchases thus far. Returns the new purchases.
     */
    private List<Integer> samplePurchase(List<Integer> purchases,
                                         List<Integer> returns) {
        List<Integer> newStock = generateStock(purchases, returns);
        // Determine the current number of items and items that are allowed
        // to be purchased.
        List<Integer> allowedPurchases = new ArrayList<>(stock.size());
        int numItems = 0;
        for (int i = 0; i < stock.size(); i++) {
            numItems += newStock.get(i);
            if (returns.get(i) == 0) {
                allowedPurchases.add(i);
            }
        }
        if (numItems >= store.getCapacity() || allowedPurchases.size() == 0) {
            return purchases;
        }
        // Randomly select an allowed purchase.
        int sampleIndex = ThreadLocalRandom.current().nextInt(0,
                allowedPurchases.size()); // Upper exclusive bound.
        // Increment the number of purchases for the same index.
        purchases.set(allowedPurchases.get(sampleIndex),
                purchases.get(allowedPurchases.get(sampleIndex)) + 1);
        return purchases;
    }

    /**
     * Samples an allowed return based on the order, stock and number of
     * returns thus far. Returns the new returns.
     */
    private List<Integer> sampleReturn(List<Integer> purchases,
                                       List<Integer> returns) {
        List<Integer> newStock = generateStock(purchases, returns);
        // Only allowed to return items that are already in the stock.
        List<Integer> allowedReturns = new ArrayList<>(stock.size());
        for (int i = 0; i < newStock.size(); i++) {
            if (newStock.get(i) > 0 && purchases.get(i) == 0) {
                allowedReturns.add(i);
            }
        }
        if (allowedReturns.size() == 0) {
            return returns;
        }
        // Randomly select an allowed return.
        int sampleIndex = ThreadLocalRandom.current().nextInt(0,
                allowedReturns.size()); // Upper exclusive bound.
        // Increment the number of returns for the same index.
        returns.set(allowedReturns.get(sampleIndex),
                returns.get(allowedReturns.get(sampleIndex)) + 1);
        return returns;
    }

    /**
     * Takes an order and applies it to the current state.
     *
     * @require the order must be legal.
     */
    private List<Integer> generateStock(List<Integer> purchases,
                                        List<Integer> returns) {
        List<Integer> newStock = initialiseList(stock.size(), 0);
        for (int i = 0; i < stock.size(); i++) {
            newStock.set(i, stock.get(i) + purchases.get(i) - returns.get(i));
        }
        return newStock;
    }

    /**
     * Takes an order and applies it to the current state.
     *
     * @require the order must be legal.
     */
    private List<Integer> generateOrder(List<Integer> purchases,
                                        List<Integer> returns) {
        List<Integer> newOrder = initialiseList(stock.size(), 0);
        for (int i = 0; i < stock.size(); i++) {
            newOrder.set(i, purchases.get(i) - returns.get(i));
        }
        return newOrder;
    }

    /**
     * Sample an Action and apply it to determine the resulting TransitionState.
     */
    @Override
    protected TransitionState applySampledEvent(Strategy strategy) {
        switch (strategy) {
            case EXPLOIT:
                if(!actionsValueSet.isEmpty()) {
                    return applyEventExploit();
                }
            case EXPLORE:
                if(!actionsUpdateSet.isEmpty()) {
                    return applyEventExplore();
                }
            default:
                // Sample randomly from the actions.
                Action sample = generateSampledAction();
                Action mapValue = actionsHashMap.get(sample);
                if (mapValue == null) {
                    return sample.getChild();
                } else {
                    return mapValue.getChild();
                }
        }
    }

    /**
     * Select an action that is currently considered a 'good' action. Which
     * of the 'good' actions is selected based on a weighted probability.
     */
    private TransitionState applyEventExploit() {
        // Setup the information that is required to do the probability stuff.
        double rand = new Random().nextDouble();
        int size = actionsValueSet.size();
        double weighting = (double) size * (size + 1) / 2;
        double cumulativeProbability = 0;
        // Iterator over the valueSet starting with the best value.
        Iterator<Action> valueSetIterator =
                actionsValueSet.descendingIterator();
        Action next;
        int i = 0;
        while (valueSetIterator.hasNext()) {
            next = valueSetIterator.next();
            // Increment the cumulative probability so that eventually we do
            // get a hit. The constant '2' is there to ensure that we only
            // search the first half of the list.
            cumulativeProbability += (double) 2 * (size - i) / weighting;
            if (cumulativeProbability >= rand) {
                return next.getChild();
            }
            i++;
        }
        return actionsValueSet.last().getChild();
//        // Setup the information that is required to do the probability stuff.
//        double rand = new Random().nextDouble();
//        double cumulativeProbability = 0.5;
//        // Iterator over the valueSet starting with the best value.
//        Iterator<Action> valueSetIterator =
//                actionsUpdateSet.descendingIterator();
//        Action next;
//        while (valueSetIterator.hasNext()) {
//            next = valueSetIterator.next();
//            // Increment the cumulative probability so that eventually we do
//            // get a hit. The constant '2' is there to ensure that we only
//            // search the first half of the list.
//            cumulativeProbability += (1 - cumulativeProbability) / 2;
//            if (cumulativeProbability >= rand) {
//                return next.getChild();
//            }
//        }
//        return actionsValueSet.last().getChild();
    }

    /**
     * Select an action that is currently considered a 'unexplored' action.
     * Which of the 'good' actions is selected based on a weighted probability.
     *
     * @require actionsUpdateSet must not be empty.
     */
    private TransitionState applyEventExplore() {
        // Setup the information that is required to do the probability stuff.
        double rand = new Random().nextDouble();
        int size = actionsUpdateSet.size();
        double weighting = (double) size * (size + 1) / 2;
        double cumulativeProbability = 0;
        // Iterator over the valueSet starting with the best value.
        Iterator<Action> updateSetIterator =
                actionsUpdateSet.iterator();
        Action next;
        int i = 0;
        while (updateSetIterator.hasNext()) {
            next = updateSetIterator.next();
            // Increment the cumulative probability so that eventually we do
            // get a hit. The constant '2' is there to ensure that we only
            // search the first half of the list.
            cumulativeProbability += (double) 2 * (size - i) / weighting;
            if (cumulativeProbability >= rand) {
                return next.getChild();
            }
            i++;
        }
        return actionsUpdateSet.first().getChild();
    }

    /**
     * Returns the set of transition states accessible from the
     * statespace. To be used for OfflineSolutions.
     */

    public ArrayList<TransitionState> getTransitionStates() {
        ArrayList<TransitionState> ret = new ArrayList<>();
        for (Action action : actionsHashMap.values()) {
            TransitionState transState = action.getChild();
            ret.add(transState);
        }
        return ret;
    }

    /**
     * Calculates and sets the best action in the state's ActionSpace
     * To be used for Offline Solutions
     */
    public void setBestAction(HashMap<List<Integer>, ActionState> stateSpace,
                              List<List<Integer>> customerDemand) {

        /* Generate the action space of the state */
        this.generateAllActions();

        /* Create placeholders for bestAction and bestValue */
        TransitionState transState;
        Action bestAction = null;
        double bestValue = Double.NEGATIVE_INFINITY;

        /* Iterate through the action space */
        for (Action action : actionsHashMap.values()) {

            /* Calculate and set the value of the action state's child */
            transState = action.getChild();
            transState.setValue(transState.calculateOfflineValue(stateSpace,
                    customerDemand));

            double newValue = action.getProfit() + transState.getValue();

            if (newValue > bestValue) {
                bestAction = action;
                bestValue = newValue;
            }
        }
        this.stateUpdate++;
        this.setValue(bestValue);
        this.bestAction = bestAction;
    }

    /**
     * Returns the order associated with the best action.
     * To be used for offline solution
     */
    public List<Integer> offlineGetBestOrder() {
        return this.bestAction.getDemand();
    }

    /**
     * Executes the best action of the actionstate and returns the resulting
     * transition state
     *
     * @require: setBestAction() has been ran previously
     */
    public TransitionState doBestAction() {
        return this.bestAction.getChild();
    }

    /**
     * Updates the position of e in the data-structures by removing it and
     * adding it again.
     *
     * @return the best value of this.
     * @require e is an Action.
     */
    @Override
    protected double updateEvents(Event e) {
        Action a = (Action) e;
        a.update();
        // Add the action.
        addAction(a);
        return actionsValueSet.last().getValue();
    }

    /**
     * Removes action from all the ordered collections.
     */
    public void removeEvent(Event event) {
        if (event instanceof Action) {
            Action action = (Action) event;
            Action mapAction = actionsHashMap.get(action);
            if (mapAction != null) {
                // Remove mapAction from the sets.
                actionsValueSet.remove(mapAction);
                actionsUpdateSet.remove(mapAction);
                actionsHashMap.remove(action);
//            if(actionsValueSet.remove(mapAction)) {
//                System.out.println();
//            } else {
//                System.out.println();
//            }
//        } else {
//            System.out.println();
                // Update the number of updates.
            }
        }
    }

    /**
     * Adds a to all the ordered collections.
     */
    private void addAction(Action a) {
        actionsValueSet.add(a);
        actionsUpdateSet.add(a);
        actionsHashMap.put(a, a);
    }

    /**
     * Checks whether this contains Action. Returns true if it does.
     */
    protected boolean containsAction(Event e) {
        if (!(e instanceof Action)) {
            return false;
        }
        Action a = (Action) e;
        for (Action action : actionsValueSet) {
            if (action.equals(a)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns stock.
     */
    @Override
    protected List<Integer> getStock() {
        return new ArrayList<>(stock);
    }

    /**
     * Gets the best order.
     */
    public List<Integer> getBestOrder() {
        return actionsValueSet.last().getDemand();
    }

    /**
     * Generates all the Actions that can be made from this.
     */
    public void generateAllActions() {
        List<Integer> ret = initialiseList(store.getMaxTypes(), 0);
        generateAllReturns(ret, 0, EMPTY_TYPE);
    }

    /**
     * Recursively generates all returns that can be made from this. i is the
     * number of returns that have already been made.
     */
    private void generateAllReturns(List<Integer> ret, int i, int start) {
        if (i >= store.getMaxReturns()) {
            // Generate the orders.
            List<Integer> buy = initialiseList(store.getMaxTypes(), 0);
            generateAllPurchases(buy, 0, EMPTY_TYPE, ret);
            return;
        }

        // Handle returning an element of every type allowing multiple
        // returns of the same item. j is the item type.
        for (int j = start; j < store.getMaxTypes(); j++) {
            List<Integer> next = new ArrayList<>(ret);
            if (j == -1) {
                generateAllReturns(next, i + 1, j);
            } else {
                next.set(j, next.get(j) + 1);
                if (returnsAllowed(next, j)) {
                    generateAllReturns(next, i + 1, j);
                }
            }
        }
    }

    /**
     * Returns true if the returns proposed are allowed based on stock.
     *
     * @require all returns up until the last element have already been checked.
     */
    private boolean returnsAllowed(List<Integer> nextRet, int i) {
        // Check that we aren't returning more than what we have of item i.
        return nextRet.get(i) <= stock.get(i);
    }

    /**
     * Recursively generates all orders that can be made with the given returns.
     */
    private void generateAllPurchases(
            List<Integer> buy, int i, int start, List<Integer> ret) {
        if (i >= store.getMaxPurchase()) {
            Action a = new Action(this, buy, ret);
            addAction(a);
            return;
        }

        // Handle returning an element of every type allowing multiple
        // orders of the same item.
        for (int j = start; j < store.getMaxTypes(); j++) {
            List<Integer> next = new ArrayList<>(buy);
            if (j == -1) {
                generateAllPurchases(next, i + 1, j, ret);
            } else {
                next.set(j, next.get(j) + 1);
                if (purchasesAllowed(next, ret, j)) {
                    generateAllPurchases(next, i + 1, j, ret);
                }
            }
        }
    }

    /**
     * Checks whether we were already returning item i and then check we have
     * not exceeded the store's capacity.
     */
    private boolean purchasesAllowed(
            List<Integer> buy, List<Integer> ret, int lastPurchaseIndex) {
        // Check we are not ordering something that has been returned.
        if (ret.get(lastPurchaseIndex) > 0) {
            return false;
        }
        // Check we are not going over capacity.
        int numItems = 0;
        for (int j = 0; j < buy.size(); j++) {
            numItems += stock.get(j) + buy.get(j) - ret.get(j);
        }
        return numItems <= store.getCapacity();
    }

    /**
     * Checks whether this has the same span over its action space as as1.
     */
    public boolean checkActionSpaceSpan(ActionState as1) {
        return as1.actionsHashMap.size() == this.actionsHashMap.size();
    }

    /**
     * Checks the invariant has not been violated.
     */
    public boolean checkInvariant() {
        // There should be no duplicates in actionsValueSet and the capacity
        // should not be exceeded for each action.
        for (Action a1 : actionsValueSet) {
            // Check duplicates in action set. One allowed (itself) per a1.
            boolean foundSelf = false;
            for (Action a2 : actionsValueSet) {
                try {
                    // Check whether the actions have the same
                    // purchases/returns.
                    if (a1.hasSameOrderReturn(a2)) {
                        if (!foundSelf) {
                            foundSelf = true;
                        } else {
                            System.out.println();
                            System.out.println("Duplicates");
                            return false;
                        }
                    }
                } catch (Exception e) {
                    System.out.println("The size of the actions didn't align.");
                }
            }
            // There should be at least one copy of this in the set.
            if (!foundSelf) {
                return false;
            }
            // Check for exceeding the capacity.
            if (!a1.checkCapacity()) {
                System.out.println("Capacity exceeded.");
                return false;
            }
        }
        // There should be no duplicates in actionsUpdateSet and the capacity
        // should not be exceeded for each action.
        for (Action a1 : actionsUpdateSet) {
            // Check duplicates in action set. One allowed (itself) per a1.
            boolean foundSelf = false;
            for (Action a2 : actionsUpdateSet) {
                try {
                    // Check whether the actions have the same
                    // purchases/returns.
                    if (a1.hasSameOrderReturn(a2)) {
                        if (!foundSelf) {
                            foundSelf = true;
                        } else {
                            System.out.println();
                            System.out.println("Duplicates");
                            return false;
                        }
                    }
                } catch (Exception e) {
                    System.out.println("The size of the actions didn't align.");
                }
            }
            // There should be at least one copy of this in the set.
            if (!foundSelf) {
                return false;
            }
            // Check for exceeding the capacity.
            if (!a1.checkCapacity()) {
                System.out.println("Capacity exceeded.");
                return false;
            }
        }
        // Check that all the action datastructures have the same size.
        if (actionsValueSet.size() != actionsHashMap.size() ||
                actionsUpdateSet.size() != actionsHashMap.size()) {
            return false;
        }
        return true;
    }

    /**
     * The Event that is used to proceed from an ActionState to a
     * TransitionState.
     */
    private class Action extends Event implements Comparable {

        /**
         * List of the purchases.
         */
        private List<Integer> purchases;
        private int numPurchases = 0;
        /**
         * List of the returns.
         */
        private List<Integer> returns;
        private int numReturns = 0;
        /**
         * List of the orders. This is the combination of returns and
         * purchases.
         */
        private List<Integer> orders;

        /**
         * The child of this.
         */
        private TransitionState child;

        /**
         * The profit made by performing this.
         */
        private double profit = 0;

        /**
         * Number of times this has been updated.
         */
        private int updates = 0;

        private int hash;

        /**
         * Constructs an instance of this using inputted purchases and
         * returns.
         *
         * @require the purchases and returns should come in the form of
         * {1,2,0}
         * where the element at index i is the number of items that should be
         * ordered/returned for that type && purchases.size() == returns
         * .size()
         */
        protected Action(ActionState parent, List<Integer> purchases,
                         List<Integer> returns) {
            super(parent);
            setReturns(returns);
            setPurchases(purchases);
            for (int i = 0; i < purchases.size(); i++) {
                numPurchases += purchases.get(i);
                numReturns += returns.get(i);
            }
            generateOrders();
            setProfit(getProblemSpec());
            setChild();
        }

//        /**
//         * Copy constructor.
//         */
//        protected Action(Action a) {
//            super(a);
//            this.purchases = a.purchases;
//            this.returns = a.returns;
//            this.numPurchases = a.numPurchases;
//            this.numReturns = a.numReturns;
//            this.orders = a.orders;
//            this.hash = a.hash;
//            this.profit = a.profit;
//            this.child = a.child;
//        }

        /**
         * Gets the order.
         */
        protected List<Integer> getDemand() {
            return new ArrayList<>(orders);
        }

        /**
         * Sets the purchases.
         */
        void setPurchases(List<Integer> purchases) {
            this.purchases = purchases;
        }

        /**
         * Generates the orders based on the returns and purchases.
         */
        private void generateOrders() {
            orders = initialiseList(purchases.size(), 0);
            for (int i = 0; i < purchases.size(); i++) {
                orders.set(i, purchases.get(i) - returns.get(i));
            }
        }

        /**
         * Sets the returns.
         */
        void setReturns(List<Integer> returns) {
            this.returns = returns;
        }

        /**
         * Checks whether this and a have the same purchases and returns.
         */
        boolean hasSameOrderReturn(Action a) throws Exception {
            if (this.purchases.size() != a.purchases.size() ||
                    this.returns.size() != a.returns.size()) {
                throw new Exception("All states should have the same number " +
                        "size of purchases and returns.");
            }

            for (int i = 0; i < this.purchases.size(); i++) {
                if (!this.purchases.get(i).equals(a.purchases.get(i))) {
                    return false;
                }
            }
            for (int i = 0; i < this.returns.size(); i++) {
                if (!this.returns.get(i).equals(a.returns.get(i))) {
                    return false;
                }
            }
            return true;
        }

        /**
         * Sets child.
         */
        @Override
        protected void setChild() {
            this.child = generateChild();
        }

        /**
         * Gets child.
         */
        @Override
        protected TransitionState getChild() {
            return child;
        }

        /**
         * Generates the child of this.
         *
         * @require the purchases and returns were generated to be applied to
         * stock without contradicting the rules.
         */
        TransitionState generateChild() {
            List<Integer> newStock = getParent().getStock();
            for (int i = 0; i < stock.size(); i++) {
                newStock.set(i,
                        newStock.get(i) - returns.get(i) + purchases.get(i));
            }
            return new TransitionState(getProblemSpec(), newStock, this);
        }

        /**
         * The hashcode of this. This hashcode does not use value.
         */
        @Override
        public int hashCode() {
            int hash = 19;
            hash = hash * 65537 + orders.hashCode();
            this.hash = hash * 65537;
            return hash;
        }

        /**
         * Equals for this.
         */
        @Override
        public boolean equals(Object o) {
            if (!(o instanceof Action)) {
                return false;
            }
            Action a = (Action) o;
            return a.hashCode() == this.hashCode();
        }

        /**
         * Implemented for the Comparable interface such that Actions with a
         * higher value are naturally ordered higher.
         */
        @Override
        public int compareTo(Object o) {
            if (!(o instanceof Action)) {
                return -1;
            }
            Action a = (Action) o;
            if (a.getValue() == null || this.getValue() == null) {
                // Order by hashcode if there is no value.
                return a.hashCode() - this.hashCode();
            }
            if (Math.abs(a.getValue() - this.getValue()) < 1e-12) {
                // Order by hashcode if values are same.
                return a.hashCode() - this.hashCode();

            }
            return this.getValue() - a.getValue() > 0 ? 1 : -1;
        }

        /**
         * Sets the profit gained by performing this action.
         */
        @Override
        protected void setProfit(ProblemSpec ps) {
            profit = 0;
            for (int i = 0; i < ps.getStore().getMaxTypes(); i++) {
                profit = profit - returns.get(i) * ps.getPrices().get(i) *
                        Constants.RETURN_FACTOR;
            }
        }

        @Override
        protected double getProfit() {
            return profit;
        }

        /**
         * Check that the actions applied by this does not exceed the
         * capacity. Returns true if we are under capacity.
         */
        boolean checkCapacity() {
            int numItems = 0;
            for (Integer i : child.getStock()) {
                numItems += i;
            }
            return numItems <= getProblemSpec().getStore().getCapacity();
        }

        /**
         * Gets the number of times this has been updated.
         */
        public int getUpdates() {
            return updates;
        }

        /**
         * Increments the number of times this has been updated.
         */
        public void update() {
            getParent().removeEvent(this);
            updates++;
        }
    }

    /**
     * Gets the number of updates that were recorded on the children.
     */
    public int getChildrenUpdates() {
        int updates = 0;
        for (Action a : actionsUpdateSet) {
            updates += a.updates;
        }
        return updates;
    }

    /**
     * A comparator for the values of action.
     */
    private class ActionValueComparator implements Comparator<Action> {

        @Override
        public int compare(Action a1, Action a2) {
            if (a1.getValue() == null || a2.getValue() == null) {
                // Order by hashcode if there is no value.
                return a1.hashCode() - a2.hashCode();
            }
            if (a1 == a2) {
                return 0;
            }
            if (Math.abs(a1.getValue() - a2.getValue()) < 1e-12) {
                // Order by hashcode if values are same.
                return a1.hashCode() - a2.hashCode();

            }
            return a1.getValue() - a2.getValue() > 0 ? 1 : -1;
        }
    }

    /**
     * A comparator for the number of updates performed on two actions.
     */
    private class ActionUpdateComparator implements Comparator<Action> {
        @Override
        public int compare(Action a1, Action a2) {
            if (a1 == a2) {
                return 0;
            }
            if (a1.updates == a2.updates) {
                // Order by hashcode if values are same.
                return a1.hashCode() - a2.hashCode();

            }
            return a1.updates - a2.updates > 0 ? 1 : -1;
        }
    }
}

