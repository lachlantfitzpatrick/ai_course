---
title: COMP3702 Assignment 3 Report
date: October 28, 2016
documentclass: ieeeconf
author: >
    \authorblockN{Lachlan Fitzpatrick} 
    \authorblockA{
    The University of Queensland\\
    Bunbois}
    \and
    \authorblockN{Hirotsugu Takahashi}
    \authorblockA{
    The University of Queensland\\
	Bunbois}
classoption: conference, letterpaper, onecolumn
linestretch: 1.25
links-as-notes: true
linkcolor: NavyBlue
numbersections: true
bibliography: ./references.bib
paper-size: A4
fontsize: 12pt
header-includes:
  - \usepackage{float}
  - \usepackage{fullpage}
  - \usepackage{hyperref}
  - \usepackage{graphicx}
  - \usepackage{subcaption}
  - \usepackage[utf8]{inputenc}
---

# Introduction 

**Explaination is 80% of the total points**

**A strong argument must include:**

 * A logical explaination of why
 * Experimental results to back your explaination


**What is a Markov Decision Process?**

A state $S_t$ is Markov if and only if:
\begin{center}
	$\mathbb{P} [ S_{t+1} \mid S_t ] = \mathbb{P} [ S_{t+1} \mid S_1, S_2, ... S_t ]$
\end{center}

That is the state captures all relevant information from the history

**Please define your MDP problem**

A sequential decision problem for a fully observable, stochastic environment with a
Markovian transition model and additive rewards is called a Markov Decision Process
[@rnorvig2010]. A Markov Decision Process - often denoted as 
$(S, A_{s}, R(s,a), T(s', s, a), \gamma)$ - can be broken down into five components:

 * Statespace, $S$
 * Actionspace, $A_{s}$
 * Reward Function, $R(s, a)$
 * Transition Function, $T(s', s, a)$
 * Discount Factor, $\gamma$

The *statespace* ($S$) represents the set of all possible states in the problem.
The *actionspace* ($A_{s}$) is the set of all actions available from a state $s \in S$. 
The *reward function* ($R(s, a)$) describes the immediate reward received by an agent
executing an action.
The *transition function* ($T(s', s, a)$) describes the probability of arriving in a 
particular state $s'$ from state $s$, taking action $a$.
Finally the *discount factor* ($\gamma$) describes an agent's preference for current 
rewards over future rewards[@rnorvig2010].

We used the Markov Decision Process framework in order to develop, UQR's inventory
management system. We broke down the inventory management problem in the following manner:

**Statespace**

The statespace is the set containing all combinations of stock that a store can have. 
The size of the statespace is constrained by the stocking capacity of the store, which ise 
determined by its size (tiny, small, medium, large or mega).

As an example, consider the statespace of a tiny sized store. 
A tiny store sells 2 types of items (item x and item y) and can stock at most
3 items. We represent its statespace as the following: 
$$
S = \{ (0, 0), (0, 1), (0, 2), (0, 3), (1, 0), (1, 1), (1, 2), (2, 0), (2, 1), (3, 0) \}
$$ 
Each element in the statespace $S$ represents a store's state.
The state $(n_x, n_y)$ represents a store's stock that consists of $n_x$ item $x$s 
and $n_y$ item $y$s.

**Actionspace:**

The actionspace of a particular state represents  the set of all actions - consisting 
of purchases and returns - that we can make given a state's stock. 

Given a certain state of a store, we generate it's actionspace by generating the 
combinations of all item returns and item purchases under the following constraints:

 1. The number of item returns and item purchases made do not exceed the maximum number 
 for the store
 2. The total number of items in stock does not exceed the maximum capacity of the store 
 at any point in time
 3. The stock of a particular item cannot become less than 0
 
As a concrete example this, consider again the tiny store example. The tiny store sells 
2 types of items, has a max stock capacity of 3 items, can order up to 2 items per week 
and can return at most 1 item per week. Suppose that the current state is state (1, 0). 

If we generate the combinations of item purchases and item returns with only the max 
returns and purchases in mind (constratint 1), the cardinality of the actionspace is 
given by the following:

$$
\text{Combinations with repetitions} = \frac{(r + k - 1)!}{r! \cdot (k - 1)!}
$$

By applying the other two constraints, the actionspace generated from this 
state becomes the following:
$$
A_s = \{(-1, 0), (0, 0), (0, 1), (1, 0), (1, 1), (2, 0), (0, 2) \}
$$ 

**Reward:**

```java
	public void main(String args[]) {
		System.out.println("Hello World!");
	}
```

	- The cost incurred from returning an item back to HQ
	- The expected immediate reward from the transactions that may occur

**Transition:**

	- The probability of arriving in state $s'$ after taking action $a$ from state $s$
	- at the next time interval

**Please explain your algorithm for solving the problem**

**Please analyse your algorithm's accuracy and speed, via comparison study.
For this purpose, at the very least, you need to compare your method against
the vase value iteration with synchronous update (as described in class) on
the 3 tiny and small problesm in the input example.**


|  Size  | Type | Capacity |  Statespace  | Order | Return |  Actionspace (C1, C2) |
|:------:|:----:|:--------:|:------------:|:-----:|:------:|:---------------------:|
| Tiny   |  2   |     3    |     10       |   2   |   1    |           18          |
| Small  |  2   |     8    |     45       |   2   |   1    |           17          | 
| Medium |  3   |     8    |     165      |   2   |   1    |           200         |
| Large  |  5   |     10   |     3003     |   2   |   1    |           2646        |
| Mega   |  8   |     20   |     888030   |   2   |   1    |           95040       |


# Bibliography
