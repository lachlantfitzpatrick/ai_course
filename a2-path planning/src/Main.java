import problem.ArmConfig;
import problem.ProblemSpec;
import problem.SolutionGenerator;
import tester.Tester;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Generates a solution to the given problem.
 * <p>
 * Created by lachlan on 7/09/16.
 */
public class Main {

    /**
     * Runs and controls the generation of the solution.
     */
    public static void main(String[] args) {

        String problemFile = args[0];
        String solutionFile = args[1];

        long tick = System.currentTimeMillis();
        try {
            //TreeA workspace
            ProblemSpec psA = new ProblemSpec();
            psA.loadProblem(problemFile);
            Tester testerA = new Tester(psA);

            //TreeB workspace
            ProblemSpec psB = new ProblemSpec();
            psB.loadProblem(problemFile);
            Tester testerB = new Tester(psB);

            SolutionGenerator sg = new SolutionGenerator();

            ArrayList<ArmConfig> path = sg.generateSolution(testerA, testerB,

                    solutionFile);

            System.out.println("All done!");
            System.out.println(System.currentTimeMillis() - tick);

            testerA.getProblemSpec().setPath(path);

        } catch (IOException ioe) {
            System.out.printf("Something has gone wrong when loading the " +
                    "file!\n");
        }

    }
}
