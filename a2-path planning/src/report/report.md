---
title: "COMP3702 Artificial Intelligence Assignment 2 Report"
author: Lachlan Fitzpatrick & Hirotsugu Takahashi
date: September 24, 2016
documentclass: article
linestretch: 1
links-as-notes: true
linkcolor: NavyBlue
toc: true
paper-size: A4
header-includes:
  - \usepackage{float}
  - \usepackage{hyperref}
  - \usepackage{graphicx}
  - \usepackage{subcaption}
  - \usepackage[utf8]{inputenc}
---

##Project Description

In this report, we outline a motion planning algorithm for a multi-degree
of freedom robot consisting of a wheelchair and a robotic arm. Our algorithm
makes use of the Rapidly-exploring Random Tree algorithm to navigate through
continuous space. 

##Background Information
The configuration space of the problem is the set of all allowable moves.
This is the union between the set of moves for the base, joints and 
grippers.

The set of moves for the base is:
$$
S_C = S_B\, \cup \, S_\theta \,\cup\,S_G
$$

The of moves for the joints are:
$$
S_C = \left \{ u_1,l_1,u_2,l_2 \, \epsilon \, ${\rm I\!R}$^4 \, | \, u_i=[0.03,0.07], \,l_i=[0.03,0.07] \right \} \}
$$

The gripper moves are only defined when there is a gripper.
The set of moves for a gripper link is:
$$
\left \{ x,y \, \epsilon \, ${\rm I\!R}$^2 \, | x=[0,1], y=[0,1] \right \} \}
$$

The total configuration space is therefore defined as:

##Path Planning Algorithm Overview
**Describe which method you used for searching in continuous space**



**If you used a sampling-based method, please describe the strategy
you applied or developed for each of its four components**

A sampling-based path planning method can be broken down into four
stages:

 * Sampling strategy
 * Configuration checking
 * Line-segment checking 
 * Connection strategy


Rapidly-exploring Random Tree


###Strategy implemented for the four components
- Sampling Strategy
    - Multi-armed bandit
        - Exploit
        - Corridor
        - Random

- Check if configuration is valid
    - Collision checking

- Check if line segment in C-space is valid
    - Motion.move

- Connection strategies(adding edges)
    - 

##Evaluation of Algorithm

###Which class of scenarios do you think you program will be able to solve?

1. Wheelchair only consists of chair

2. Wheelchair consists of chair and arm

3. Wheelchair has chair, arm and grippers
    - 6+ arms


###Under what situation do you think your program will fail?
1. Wheelchair only consists of chair

2. Wheelchair consists of chair and arm

3. Wheelchair has chair, arm and grippers
    - 6+ arms


#Bibliography
