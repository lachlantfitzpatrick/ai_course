package problem;

import tester.Tester;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.*;
import java.util.List;

/**
 * Created by lachlan on 8/09/16.
 */
public class SolutionGenerator {

    /**
     * A series of parameters that can be used to control the solver's
     * strategy.
     */
    public static double MAX_WORKSPACE_SIZE = 0.05;

    // Maximum width of corridors
    public static final double MAX_CORRIDOR_WIDTH = 0.3;
    // Minimum width of corridors
    public static final double MIN_CORRIDOR_WIDTH = 0.04;

    //Maximum joint angle
    public static double MAX_ANGLE = 150;

    //Maximum gripper length
    public static double MAX_GRIPPER_LENGTH = 0.07;
    public static double MIN_GRIPPER_LENGTH = 0.03;

    //Gripper values
    public static int GRIPPER_PARTS = 4;

    // Weights for cost function
    public static final double EXPLORATION_WEIGHT = 0;
    public static final double EXPLOITATION_WEIGHT = 1;
    public static final double OBSTACLE_WEIGHT = 0;

    // Likelihood that base will be sampled with an exploit strategy
    public static final double TO_FINAL_GOAL = 0.4;
    // Initial likelihood that base will be sampled with an exploit strategy
    public static double TO_GOAL = 0.02;
    // Final likelihood that base will be sampled with an exploit strategy
    // after all of the subgoals have been reached.
    public static final double SECOND_TO_GOAL = 0.3;
    // Probability that base will be sampled with a subgoal strategy.
    private static final double TO_SUBGOAL = 0.2;
    // Likelihood that sampling will be random as opposed to in corridors
    public static final double RANDOM_VS_OBSTACLE = 0.7;

    // RRT translation step size
    public static final double STEP_SIZE = 0.05;

    // RRT angle step size when interpolating (degrees).
    public static final double ANGLE_STEP = 2.2;

    // Distance that defines a 'nearby' corridor.
    public static final double NEARBY = 0.05;

    // Distance that defines when we are beginning to approach the goal.
    public static final double APPROACH_DISTANCE = 0.2;

    // Probability of resizing the grippers to the smallest ones.
    public static final double KEEP_SMALL = 0.99;

    // Number of nearby corridor samples to take.
    public static final int NUM_SUB_GOALS = 15;
    // A scaling factor used to determine how frequently to generate the
    // subgoal in a corridor.
    private static final double FREE_SPACE_SCALING = 2.0;
    // The distance to a subgoal before it is declared reached.
    private double SUBGOAL_REACHED = 0.1;

    // The last nodes that were expanded in the respective trees.
    private ArmConfig lastA;
    private ArmConfig lastB;

    // The number of turns that have been taken thus far.
    int turn;

    // A series of points that the trees will try and reach as a sub-goal of
    // completing the problem.
    private ArrayList<ArmConfig> subgoals;
    // The last index that was taken from subgoals.
    private int lastSampled = 0;

    // The last strategy that was used.
    private Strategy lastStrategy = Strategy.RANDOM;


    /**
     * Takes in two problem spaces. One for tree A and one for treeB and
     * generates a primitive movement path.
     * Tree A grows from the initial configuration.
     * Tree B grows from goal configuration.
     * When tree A connects to B, the path is generated
     */
    public ArrayList<ArmConfig> generateSolution(Tester testerA, Tester testerB,
                                                 String outputFilename) {

        ArrayList<ArmConfig> path = new ArrayList<>();
        ArrayList<ArmConfig> samples = new ArrayList<>();

        // Preprocess the workspace for areas of interest
        ArrayList<Rectangle2D> regionsOfInterest = preProcessWorkspace(testerA);
        // Generate the subgoals.
        subgoals = generateSubgoals(testerA, regionsOfInterest);

        samples.addAll(subgoals);

        //Create trees
        ArrayList<ArmConfig> treeA = new ArrayList<>();
        ArrayList<ArmConfig> treeB = new ArrayList<>();

        //Add initial configuration to treeA and goal configuration to treeB
        ArmConfig initialState = testerA.getProblemSpec().getInitialState();
        ArmConfig goalState = testerB.getProblemSpec().getGoalState();

        treeA.add(initialState);
        testerA.getProblemSpec().getWorkspace().addNode(initialState);
        lastA = initialState;

        treeB.add(goalState);
        testerB.getProblemSpec().getWorkspace().addNode(goalState);
        lastB = goalState;

        ArmConfig candidate, sample, nearest;
        ArrayList<ArmConfig> keys;
        boolean atGoal = false;
        turn = 0;
        int counter = 0;
        Random rand = new Random();
        // Loop while we are not at goal.
        while (!atGoal) {
            turn = turn % 2;
            if (turn == 0) {
                //TreeA's turn
                //Sample the workspace
                if (rand.nextDouble() < TO_FINAL_GOAL) {
                    sample = sampleWithStrategy(testerA, regionsOfInterest,
                            goalState);
                } else {
                    sample = sampleWithStrategy(testerA, regionsOfInterest,
                            lastB);
                }
                //Determine the nearest node on treeA to sample
                nearest = testerA.getProblemSpec().getWorkspace()
                        .findClosestNode(sample);
                //Extend treeA
                candidate = extendTowardsSample(sample, nearest, testerA,
                        treeA);
                //If the tree extension is successful (candidate is not null),
                // attempt a connection to treeB
                if (candidate != null) {
                    samples.add(candidate);
                    keys = attemptTreeConnection(candidate, testerA, treeA,
                            testerB);
                    if (keys != null) {
                        atGoal = true;
                        path = backTrace(keys, testerA, treeA, treeB,
                                outputFilename);
                        break;
                    }
                    // Candidate was last added to tree.
                    lastA = candidate;
                    // Check whether to remove the subgoal.
                    checkSubgoal(candidate);
                }
            } else {
                //TreeB's turn
                //Sample the workspace
                if (rand.nextDouble() < TO_FINAL_GOAL) {
                    sample = sampleWithStrategy(testerB, regionsOfInterest,
                            initialState);
                } else {
                    sample = sampleWithStrategy(testerB, regionsOfInterest,
                            lastA);
                }
                //Determine the nearest node on treeB to sample
                nearest = testerB.getProblemSpec().getWorkspace()
                        .findClosestNode(sample);
                //Extend treeB
                candidate = extendTowardsSample(sample, nearest, testerB,
                        treeB);
                //If the tree extension is successful, attempt a connection
                // to treeA
                if (candidate != null) {
                    samples.add(candidate);
                    keys = attemptTreeConnection(candidate, testerB, treeB,
                            testerA);
                    if (keys != null) {
                        atGoal = true;
                        path = backTrace(keys, testerA, treeA, treeB,
                                outputFilename);
                        break;
                    }
                    // Candidate was last added to tree.
                    lastB = candidate;
                    // Check whether to remove the subgoal.
                    checkSubgoal(candidate);
                }
            }
            turn++;

            if (counter % 10000 == 0) {
                testerA.getProblemSpec().setPath(samples);
                try {
                    testerA.getProblemSpec().saveSolution("" +
                            "./testcases/samples.txt");

                } catch (IOException e) {
                    System.out.println("Failed to save samples.");
                }
            }
            counter++;
        }
        System.out.printf("Solution found!\n");
        return path;
    }

    /**
     * Checks whether a subgoal needs to be removed.
     */
    private void checkSubgoal(ArmConfig candidate) {
        if (lastStrategy == Strategy.SUBGOAL) {
            if (subgoals.get(lastSampled).getBaseCenter().distance(candidate
                    .getBaseCenter()) < SUBGOAL_REACHED) {
                subgoals.remove(lastSampled);
            }
        }
        if(subgoals.size() == 0) {
            TO_GOAL = SECOND_TO_GOAL;
        }
    }

    /**
     * Generates the subgoals that the
     */
    private ArrayList<ArmConfig> generateSubgoals(
            Tester tester, ArrayList<Rectangle2D> corridors) {

        ArrayList<ArmConfig> subgoals = new ArrayList<>();
        Random rand = new Random();

        int jointCount = tester.getProblemSpec().getInitialState()
                .getJointCount();
        ArrayList<Double> sampleJoints;
        ArrayList<Double> gripperLengths;
        Point2D basePos;
        ArmConfig sample;
        int i = 0;
        while (i < NUM_SUB_GOALS) {
            while (true) {
                sampleJoints = new ArrayList<>();
                gripperLengths = new ArrayList<>();

                // Generate the base. The number of corridors determines how
                // many samples should be taken in a corridor.
//                if (rand.nextDouble() < FREE_SPACE_SCALING / corridors.size
// ()) {
                basePos = new Point2D.Double(
                        rand.nextDouble(), rand.nextDouble());
//                } else {
//                    Rectangle2D region = corridors.get(
//                            rand.nextInt(corridors.size()));
//
//                    double maxX = region.getMaxX();
//                    double minX = region.getMinX();
//                    double maxY = region.getMaxY();
//                    double minY = region.getMinY();
//
//                    basePos = new Point2D.Double(
//                            minX + (maxX - minX) * rand.nextDouble(),
//                            minY + (maxY - minY) * rand.nextDouble());
//                }

                // Generate the joints.
                for (int j = 0; j < jointCount; j++) {
                    sampleJoints.add(((0.5 - rand.nextDouble()) * 2 *
                            MAX_ANGLE) * (Math.PI / 180));
                }

                // region Generate the gripper.
                if (tester.getProblemSpec().getInitialState().hasGripper()) {
                    double dist = basePos.distance(
                            tester.getProblemSpec().getGoalState()
                                    .getBaseCenter());
                    for (int j = 0; j < GRIPPER_PARTS; j++) {
                        if (dist > SolutionGenerator.APPROACH_DISTANCE) {
                            if (Math.random() < KEEP_SMALL) {
                                gripperLengths.add(MIN_GRIPPER_LENGTH);
                            } else {
                                gripperLengths.add(MIN_GRIPPER_LENGTH +
                                        (rand.nextDouble() *
                                                (MAX_GRIPPER_LENGTH -
                                                        MIN_GRIPPER_LENGTH)));

                            }
                        } else {
                            gripperLengths.add(MIN_GRIPPER_LENGTH +
                                    (rand.nextDouble() *
                                            (MAX_GRIPPER_LENGTH -
                                                    MIN_GRIPPER_LENGTH)));
                        }
                    }
                }
                // endregion

                if (tester.getProblemSpec().getInitialState().hasGripper()) {
                    sample = new ArmConfig(basePos, sampleJoints,
                            gripperLengths);
                } else {
                    sample = new ArmConfig(basePos, sampleJoints);
                }

                // Check for collisions.
                if (!sample.checkCollision(tester)) {
                    subgoals.add(sample);
                    i++;
                    break;
                }
            }
        }
        return subgoals;
    }

    /**
     * Attempt the connection from one tree to another
     */
    public ArrayList<ArmConfig> attemptTreeConnection(
            ArmConfig candidate, Tester currentTester,
            ArrayList<ArmConfig> currentTree, Tester otherTester) {

        ArrayList<ArmConfig> keys = new ArrayList<>();
        boolean attemptConnection = true;
        ArmConfig interpolation;
        ArmConfig goalCopy;

        //Keep making connections until you fail
        while (attemptConnection) {

            //Find the node from the other tree that is nearest to the
            // candidate node
            ArmConfig otherTreeNode = otherTester.getProblemSpec()
                    .getWorkspace().findClosestNode(candidate);

            //If you can step directly to it, do it
            double distance = otherTreeNode.getBaseCenter().distance
                    (candidate.getBaseCenter());
            if (distance < STEP_SIZE) {
                //Get a fresh copy of otherTreeNode, without the parents,
                if (candidate.hasGripper()) {
                    goalCopy = new ArmConfig(otherTreeNode.getBaseCenter(),
                            otherTreeNode.getJointAngles(),
                            otherTreeNode.getGripperLengths());
                } else {
                    goalCopy = new ArmConfig(otherTreeNode.getBaseCenter(),
                            otherTreeNode.getJointAngles());
                }
                candidate = directConnection(candidate, goalCopy,
                        currentTester, currentTree);

                //Did the trees connect?
                if (candidate != null) {
                    //The tree connected! Return the keys (otherTreeNode and
                    // its copy, goalCopy);
                    keys.add(otherTreeNode);
                    keys.add(goalCopy);
                    return keys;
                } else {
                    //If you can't make the direct connection to the tree
                    // yet, sample more.
                    //That is, stop trying to connect for now. Try again later.
                    attemptConnection = false;
                }
                //If you can't step to it.
                // Get the interpolation between the nearest point in the
                // other tree and the
                // Node you just connected
            } else {
                interpolation = getInterpolation(candidate, otherTreeNode);
                //and connect to it
                candidate = directConnection(candidate, interpolation,
                        currentTester, currentTree);
                if (candidate == null) {
                    //If you can't, then stop attempting
                    attemptConnection = false;
                }
            }
        }
        return null;
    }

    /**
     * Extends tree towards a sample.
     * If the sample is within a step size away of the nearest node in the tree,
     * it will attempt a direct connection.
     * Otherwise it will attempt to connect with the interpolation between the
     * nearest node and the sample.
     * <p>
     * If a successful connection is made, a valid Armconfig is returned,
     * otherwise, a null value is returned.
     */
    public ArmConfig extendTowardsSample(
            ArmConfig sample, ArmConfig nearest, Tester tester,
            ArrayList<ArmConfig> tree) {

        ArmConfig candidate;

        // Connect to sampled if it's within the step range
        double distance = nearest.getBaseCenter().distance(sample
                .getBaseCenter());
        if (distance < STEP_SIZE) {
            candidate = directConnection(nearest, sample, tester, tree);
            return candidate;
        }
        // Otherwise create the interpolation between
        // nearest and sample and connect to that
        ArmConfig interpolation = getInterpolation(nearest, sample);
        candidate = directConnection(nearest, interpolation, tester, tree);
        return candidate;
    }

    /**
     * Sample with the three-armed bandit strategy
     */
    public ArmConfig sampleWithStrategy(
            Tester tester, ArrayList<Rectangle2D> regionsOfInterest,
            ArmConfig target) {

        Random rand = new Random();
        ArmConfig sample;

        // Three armed-bandit strategy
        if (rand.nextDouble() < TO_GOAL) {
            // Goal biased sampling
            sample = target;
            lastStrategy = Strategy.EXPLOIT;
        } else if (rand.nextDouble() < TO_SUBGOAL && subgoals.size() > 0) {
            sample = generateSubgoalSample();
            lastStrategy = Strategy.SUBGOAL;
        } else {
            // Obstacle sampling or Random sampling?
            if (rand.nextDouble() < RANDOM_VS_OBSTACLE || regionsOfInterest
                    .size() == 0) {
                sample = generateRandomSample(tester);
            } else {
                // Sample in a corridor near the last node.
                sample = generateObstacleSample(tester, regionsOfInterest);
            }
            lastStrategy = Strategy.EXPLORE;
        }
        return sample;
    }

    /**
     * Randomly selects a subgoal and tries to take a sample there
     */
    private ArmConfig generateSubgoalSample() {
        lastSampled = new Random().nextInt(subgoals.size());
        return subgoals.get(lastSampled);
    }

    /**
     * Returns an arm configuration that is an interpolation between start
     * and goal
     * configurations. The interpolation is conducted using:
     * - STEP_SIZE for the translation increment
     * - ANGLE_STEP for the rotation increment
     * - GRIP_STEP for the gripper size increment
     */
    public ArmConfig getInterpolation(
            ArmConfig startConfig, ArmConfig goalConfig) {

        ArmConfig interpolation;
        ArrayList<Double> interpolatedJointAngles = new ArrayList<>();
        ArrayList<Double> interpolatedGripperLengths = new ArrayList<>();
        Point2D branchBase;
        Random rand = new Random();

        double transAngle = Math.atan2(
                goalConfig.getBaseCenter().getY() - startConfig.getBaseCenter
                        ().getY(),
                goalConfig.getBaseCenter().getX() - startConfig.getBaseCenter
                        ().getX());

        // Translate in a range from 0 to STEP_SIZE towards sample
        double offset = STEP_SIZE * rand.nextDouble();
        double offsetX = offset * Math.cos(transAngle);
        double offsetY = offset * rand.nextDouble() * Math.sin(transAngle);
        branchBase = new Point2D.Double(startConfig.getBaseCenter().getX() +
                offsetX, startConfig.getBaseCenter().getY() + offsetY);

        // Move joint angles in a range from 0 to MAX
        for (int i = 0; i < goalConfig.getJointCount(); i++) {
            double deltaAngle = goalConfig.getJointAngles().get(i) -
                    startConfig.getJointAngles().get(i);
            interpolatedJointAngles.add(startConfig.getJointAngles().get(i) +
                    Math.copySign(ANGLE_STEP * (Math.PI / 180) * rand
                            .nextDouble(), deltaAngle));
        }

        if (startConfig.hasGripper()) {
            for (int i = 0; i < startConfig.getGripperLengths().size(); i++) {
                double deltaGripper = goalConfig.getGripperLengths().get(i) -
                        startConfig.getGripperLengths().get(i);
                interpolatedGripperLengths.add(startConfig.getGripperLengths
                        ().get(i) + deltaGripper * rand.nextDouble());
            }
            interpolation = new ArmConfig(branchBase,
                    interpolatedJointAngles, interpolatedGripperLengths);
        } else {
            interpolation = new ArmConfig(branchBase, interpolatedJointAngles);
        }
        return interpolation;
    }

    /**
     * Attempt a direct connection between between one configuration to another.
     * If successful, it returns a valid ArmConfig, otherwise it returns a null
     */
    public ArmConfig directConnection(
            ArmConfig startConfig, ArmConfig goalConfig,
            Tester tester, ArrayList<ArmConfig> tree) {

        try {
            ArrayList<ArmConfig> movesMade = Motion.move(startConfig,
                    goalConfig, tester);
            goalConfig.setMovesMade(movesMade);

            tester.getProblemSpec().getWorkspace().addNode(goalConfig);
            goalConfig.setParent(startConfig);
            tree.add(goalConfig);
            return goalConfig;
        } catch (CollisionDetectedException cde) {
            return null;
        }
    }

    /**
     * Generate a randomised configuration with the base at the regions of
     * interests given
     */
    public ArmConfig generateObstacleSample(
            Tester tester, ArrayList<Rectangle2D> regionsOfInterest) {

        Random rand = new Random();
        int jointCount = tester.getProblemSpec().getInitialState()
                .getJointCount();
        ArrayList<Double> sampleJoints = new ArrayList<>();
        ArrayList<Double> gripperLengths = new ArrayList<>();

        // Find a region that are near. If none generate a random sample
        // in a random corridor.
        Rectangle2D nearbyCorridor;
        if (turn == 0) {
            nearbyCorridor = getNearbyCorridor(regionsOfInterest, lastA);
        } else {
            nearbyCorridor = getNearbyCorridor(regionsOfInterest, lastB);
        }

        //Pick a random obstacle
        Rectangle2D region;
        if (nearbyCorridor != null) {
            region = nearbyCorridor;
        } else {
            // Use a random sample.
            return generateRandomSample(tester);
        }
        double maxX = region.getMaxX();
        double minX = region.getMinX();
        double maxY = region.getMaxY();
        double minY = region.getMinY();

        Point2D basePos = new Point2D.Double(minX + (maxX - minX) * rand
                .nextDouble(), minY + (maxY - minY) * rand.nextDouble());

        for (int i = 0; i < jointCount; i++) {
            sampleJoints.add(((0.5 - rand.nextDouble()) * 2 * MAX_ANGLE) *
                    (Math.PI / 180));
        }

        // Choose the gripper lengths such that they are minimised with a
        // probability until closer to the goal.
        if (tester.getProblemSpec().getInitialState().hasGripper()) {
            double dist = basePos.distance(
                    tester.getProblemSpec().getGoalState().getBaseCenter());
            for (int i = 0; i < GRIPPER_PARTS; i++) {
                if (dist > APPROACH_DISTANCE) {
                    if (Math.random() < KEEP_SMALL) {
                        gripperLengths.add(MIN_GRIPPER_LENGTH);
                    } else {
                        gripperLengths.add(
                                MIN_GRIPPER_LENGTH + (rand.nextDouble() *
                                        (MAX_GRIPPER_LENGTH -
                                                MIN_GRIPPER_LENGTH)));

                    }
                } else {
                    gripperLengths.add(MIN_GRIPPER_LENGTH + (rand.nextDouble() *
                            (MAX_GRIPPER_LENGTH - MIN_GRIPPER_LENGTH)));
                }
            }
        }

        if (tester.getProblemSpec().getInitialState().hasGripper()) {
            return new ArmConfig(basePos, sampleJoints, gripperLengths);
        } else {
            return new ArmConfig(basePos, sampleJoints);
        }

    }

    /**
     * Gets the corridors nearest to the last successful sample.
     */
    private Rectangle2D getNearbyCorridor(
            ArrayList<Rectangle2D> regionsOfInterest, ArmConfig last) {

        // Check around the corridors to see if the base lies within 'nearby'
        // of it.
        Point2D base = last.getBaseCenter();
        int start = new Random().nextInt(regionsOfInterest.size());
        for (int i = start; i < regionsOfInterest.size(); i++) {
            Rectangle2D corridor = regionsOfInterest.get(i);
            if (corridor.getMinX() - NEARBY < base.getX() &&
                    corridor.getMinY() - NEARBY < base.getY() &&
                    corridor.getMaxX() + NEARBY > base.getX() &&
                    corridor.getMaxY() + NEARBY > base.getY()) {
                return corridor;
            }
        }
        return null;
    }

    /**
     * Generate a random allowable sample
     * basePos at a certain step size from current basePos
     * Joint angles = theta | -150 < theta < 150
     * gripperLengths un | 0.03 < un < 0.07
     */
    public ArmConfig generateRandomSample(Tester tester) {

        Random rand = new Random();
        int jointCount = tester.getProblemSpec().getInitialState()
                .getJointCount();
        ArrayList<Double> sampleJoints = new ArrayList<>();
        ArrayList<Double> gripperLengths = new ArrayList<>();

        Point2D basePos = new Point2D.Double(rand.nextDouble(), rand
                .nextDouble());

        for (int i = 0; i < jointCount; i++) {
            sampleJoints.add(((0.5 - rand.nextDouble()) * 2 * MAX_ANGLE) *
                    (Math.PI / 180));
        }

        // Generate the gripper.
        if (tester.getProblemSpec().getInitialState().hasGripper()) {
            double dist = basePos.distance(
                    tester.getProblemSpec().getGoalState().getBaseCenter());
            for (int i = 0; i < GRIPPER_PARTS; i++) {
                if (dist > SolutionGenerator.APPROACH_DISTANCE) {
                    if (Math.random() < KEEP_SMALL) {
                        gripperLengths.add(MIN_GRIPPER_LENGTH);
                    } else {
                        gripperLengths.add(MIN_GRIPPER_LENGTH +
                                (rand.nextDouble() *
                                        (MAX_GRIPPER_LENGTH -
                                                MIN_GRIPPER_LENGTH)));

                    }
                } else {
                    gripperLengths.add(MIN_GRIPPER_LENGTH + (rand.nextDouble() *
                            (MAX_GRIPPER_LENGTH - MIN_GRIPPER_LENGTH)));
                }
            }
        }

        if (tester.getProblemSpec().getInitialState().hasGripper()) {
            return new ArmConfig(basePos, sampleJoints, gripperLengths);
        } else {
            return new ArmConfig(basePos, sampleJoints);
        }
    }


    /**
     * Returns a list of primitive moves made from initial configuration to
     * the goal.
     * Concatenate the "moves to" parameter in each parent from goal to initial
     */
    public ArrayList<ArmConfig> backTrace(ArrayList<ArmConfig> keys, Tester
            tester, ArrayList<ArmConfig> treeA, ArrayList<ArmConfig> treeB,
                                          String outputFilename) {

        System.out.println("BackTracing!");
        int keyIndex;
        ArmConfig current;
        ArrayList<ArmConfig> pathA = new ArrayList<>();
        ArrayList<ArmConfig> segmentA = new ArrayList<>();
        ArrayList<ArmConfig> pathB = new ArrayList<>();
        ArrayList<ArmConfig> segmentB = new ArrayList<>();

        //Backtracing for treeA
        if (treeA.contains(keys.get(0))) {
            keyIndex = treeA.indexOf(keys.get(0));
        } else {
            keyIndex = treeA.indexOf(keys.get(1));
        }
        current = treeA.get(keyIndex);
        while (current.getParent() != null) {
            segmentA = current.getMovesMade();
            pathA.addAll(segmentA);
            current = current.getParent();
        }
        Collections.reverse(pathA);

        //Backtracing for treeB
        if (treeB.contains(keys.get(0))) {
            keyIndex = treeB.indexOf(keys.get(0));
        } else {
            keyIndex = treeB.indexOf(keys.get(1));
        }
        current = treeB.get(keyIndex);
        while (current.getParent() != null) {
            segmentB = current.getMovesMade();
            pathB.addAll(segmentB);
            current = current.getParent();
        }

        //Concatenate pathB onto pathA
        pathA.addAll(pathB);

        tester.getProblemSpec().setPath(pathA);
        try {
            tester.getProblemSpec().saveSolution(outputFilename);
        } catch (IOException ioe) {
            System.out.printf("Something has gone wrong when saving the " +
                    "file!\n");
        }
        return pathA;
    }

    public void setCost(ArmConfig child, Tester tester) {
        double cost, distance, nodeDensity, obstacleDensity;

        distance = child.totalDistance(tester.getProblemSpec().getGoalState());
        //+ child.maxAngleDiff(ps.getGoalState())
        //+ child.maxGripperDiff(ps.getGoalState());

        nodeDensity = tester.getProblemSpec().getWorkspace().getNodeDensity
                (child.getBaseCenter());
        obstacleDensity = tester.getProblemSpec().getWorkspace()
                .getObstacleDensity(child.getBaseCenter());

        cost = EXPLOITATION_WEIGHT * distance
                + EXPLORATION_WEIGHT * nodeDensity
                + OBSTACLE_WEIGHT * obstacleDensity;

        child.setCost(cost);
    }


    /**
     * Pre-process the workspace for any sampling regions of interest
     */
    public ArrayList<Rectangle2D> preProcessWorkspace(Tester tester) {
        //Region of interest
        ArrayList<Rectangle2D> regionsOfInterests = new ArrayList<>();

        //The real objects list
        List<Obstacle> obstacleList = tester.getProblemSpec().getObstacles();

        Obstacle topWall = new Obstacle(-0.3, 1.0, 1.6, 0.3);
        Obstacle bottomWall = new Obstacle(-0.3, -0.3, 1.6, 0.3);
        Obstacle leftWall = new Obstacle(-0.3, -0.3, 0.3, 1.6);
        Obstacle rightWall = new Obstacle(1.0, -0.3, 0.3, 1.6);

        // Add the walls.
        obstacleList.add(topWall);
        obstacleList.add(bottomWall);
        obstacleList.add(leftWall);
        obstacleList.add(rightWall);

        for (int i = 0; i < obstacleList.size(); i++) {

            Obstacle obs1 = obstacleList.get(i);
            Rectangle2D rect1 = obs1.getRect();

            for (int j = i; j < obstacleList.size(); j++) {

                Obstacle obs2 = obstacleList.get(j);
                Rectangle2D rect2 = obs2.getRect();

                //Blanket test
                Rectangle2D blanket1 = Tester.grow(rect1, MAX_CORRIDOR_WIDTH);
                if (!(obs1.checkCollision(rect2)) &&
                        obs2.checkCollision(blanket1)) {

                    regionsOfInterests.addAll(makeCorridors(rect1, rect2));

                }
            }
        }

        return regionsOfInterests;
    }

    /**
     * Finds the 'corridors' around the rectangles. The corridors are just
     * the regions that are not taken by the rectangles. Corridors that
     * adjoin two other corridors will be extended such that both the other
     * corridors include them.
     */
    private ArrayList<Rectangle2D> makeCorridors(
            Rectangle2D rect1, Rectangle2D rect2) {
        // Treat all the rectangles as if they could be corridors and then
        // eliminate ones that aren't.
        ArrayList<Rectangle2D> subdivision = subdivide(rect1, rect2);
        ArrayList<Rectangle2D> corridors = new ArrayList<>();

        // Add corridors that are not too small or in collision.
        for (Rectangle2D r : subdivision) {
            // Check size and bounds.
            if (!((r.getHeight() < MIN_CORRIDOR_WIDTH &&
                    r.getWidth() < MIN_CORRIDOR_WIDTH)) && rectInBounds(r)) {
                // Check collision with given rectangles.
                Rectangle2D test = Tester.grow(r, -1e-12);
                if (!((test.getMinX() > rect1.getMinX() &&
                        test.getMaxX() < rect1.getMaxX() &&
                        test.getMinY() > rect1.getMinY() &&
                        test.getMaxY() < rect1.getMaxY())
                        || (test.getMinX() > rect2.getMinX() &&
                        test.getMaxX() < rect2.getMaxX() &&
                        test.getMinY() > rect2.getMinY() &&
                        test.getMaxY() < rect2.getMaxY()))) {
                    corridors.add(r);
                }
            }

        }
        return corridors;
    }

    /**
     * Returns true if the given rectangle is in the bounds of the workspace.
     */
    private boolean rectInBounds(Rectangle2D r) {
        return r.getMinX() > 0.0 && r.getMaxX() < 1.0 &&
                r.getMinY() > 0.0 && r.getMaxY() < 1.0;
    }

    /**
     * Subdivides the given rectangles up into 9 rectangles that form a
     * larger rectangle that bounds both initial rectangles.
     */
    private ArrayList<Rectangle2D> subdivide(Rectangle2D r1, Rectangle2D r2) {
        ArrayList<Double> x = new ArrayList<>();
        ArrayList<Double> y = new ArrayList<>();

        x.add(r1.getMinX());
        x.add(r1.getMaxX());
        x.add(r2.getMinX());
        x.add(r2.getMaxX());

        y.add(r1.getMinY());
        y.add(r1.getMaxY());
        y.add(r2.getMinY());
        y.add(r2.getMaxY());

        // Sort both the lists form lowest to highest.
        for (int i = 1; i < x.size(); i++) {
            for (int j = 0; j < i; j++) {
                if (x.get(j) > x.get(i)) {
                    x.add(j, x.remove(i));
                }
                if (y.get(j) > y.get(i)) {
                    y.add(j, y.remove(i));
                }
            }
        }

        // Create the rectangles that form the subdivision.
        ArrayList<Rectangle2D> subdivision = new ArrayList<>();
        for (int i = 0; i < x.size() - 1; i++) {
            for (int j = 0; j < y.size() - 1; j++) {
                subdivision.add(
                        new Rectangle2D.Double(x.get(i), y.get(j),
                                x.get(i + 1) - x.get(i), y.get(j + 1) - y.get(j)
                        ));
            }
        }
        return subdivision;
    }
}
