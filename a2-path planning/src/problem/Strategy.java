package problem;

/**
 * Created by lachlan on 8/09/16.
 */
public enum Strategy {
    EXPLOIT, EXPLORE, AVOID, RANDOM, SUBGOAL
}
