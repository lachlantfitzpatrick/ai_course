package problem;

/**
 * Created by lachlan on 5/09/16.
 */
public class CollisionDetectedException extends Exception {

    public CollisionDetectedException() {
        super();
    }

    public CollisionDetectedException(String s) {
        super(s);
    }

}
