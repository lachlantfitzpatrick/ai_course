package problem;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Random;

/**
 * Represents a square workspace and the obstacles that it contains. The
 * wheelchair is not included in the workspace. The workspace is represented
 * as a hierarchically bounded volume according to the workspace being broken
 * into 4 children until the children are approximately of the length of the
 * the wheelchair gripper. Each child will also be a square workspace.
 * <p>
 * Created by lachlan on 4/09/16.
 */
public class Workspace {

    /*
        class invariant:
            both sides of the workspace must be the same (the workspace must
            be square) &&
            p1.getX() < p2.getX() &&
            p1.getY() < p1.getY()
     */

    // The actual length of the workspace at the lowest level. Is a square.
    private static double length;
    // The number of lowest level workspace in a row/column.
    private static int gridSize;

    // The maximum length of the workspace at the bottom of the hierarchy.
    private final double finalSize = SolutionGenerator.MAX_WORKSPACE_SIZE;

    // The children of this.
    private ArrayList<Workspace> children;
    // The parent of this.
    private Workspace parent;

    // The bounding points of this workspace.
    private Point2D p1;
    private Point2D p2;

    // Indicates whether this is the isLowest level of the hierarchy.
    private boolean isLowest = false;
    private boolean isHighest = false;

    // A list of all the obstacles that this contains. This will only be
    // populated for the isLowest level of the hierarchy.
    private ArrayList<Obstacle> obstacles = null;
    // Flag to indicate whether there are obstacles in this.
    private boolean hasObstacles = false;
    // A list of ArmConfigs with their base in this.
    private ArrayList<ArmConfig> nodes;

    // The number of nodes and obstacles in this workspace.
    private int nodeDensity = 0;
    private int obstacleDensity = 0;

    // The coordinates of a workspace. Are integer numbers.
    private Point coords;

    /**
     * Creates a workspace.
     */
    public Workspace() {
        children = new ArrayList<>();
        // Bottom left corner.
        p1 = new Point2D.Double(0.0, 0.0);
        // Top right corner.
        p2 = new Point2D.Double(1.0, 1.0);

        isHighest = true;
        nodeDensity = 0;

        createChildren();

        // Give all children at lowest level their coordinates.
        setCoords();
    }

    /**
     * Creates a workspace with the given coordinates. p1 is the bottom left
     * coordinate and p2 is the top right coordinate.
     */
    private Workspace(Workspace parent, double length, Point2D p1) {
        this.parent = parent;
        this.p1 = p1;
        p2 = new Point2D.Double(p1.getX() + length, p1.getY() + length);
        children = new ArrayList<>();

        if (getLength() > finalSize) {
            createChildren();
        } else {
            isLowest = true;
            obstacles = new ArrayList<>();
            nodes = new ArrayList<>();
            Workspace.length = p2.getX() - p1.getX();
        }
    }

    /**
     * Sets the coordinates of all the lowest level workspaces.
     */
    private void setCoords() {
        // Start at a point in the middle of the bottom left workspace and
        // iterate over entire workspace.
        double x = length / 2;
        double y = length / 2;

        int i = 0;
        int j = 0;
        while (y < 1.0) {
            while (x < 1.0) {
                getLowestWorkspace(new Point2D.Double(x, y)).setCoords(i, j);
                x += length;
                i++;
            }
            x = length / 2;
            i = 0;
            y += length;
            j++;
        }
        gridSize = j;
    }

    /**
     * Sets the coordinates of this.
     */
    private void setCoords(int x, int y) {
        coords = new Point(x, y);
    }

    /**
     * Gets the node density from the workspace.
     */
    public int getNodeDensity(Point2D point) {
        return getLowestWorkspace(point).nodeDensity;
    }

    /**
     * Adds a node to the workspace. This is based purely on the coordinates
     * of the base.
     */
    public void addNode(ArmConfig newNode) {
        Point2D base = newNode.getBaseCenter();
        Workspace lowest = getLowestWorkspace(base);
        lowest.nodeDensity++;
        lowest.nodes.add(newNode);
        this.nodeDensity++;
    }

    /**
     * Gets the closest node to given point.
     */
    public ArmConfig findClosestNode(ArmConfig goal) {
        Point2D goalBase = goal.getBaseCenter();
        Workspace target = getLowestWorkspace(goalBase);

        // Find the closest workspace. Perform this as a binary search in
        // both X and Y direction simultaneously.
        int targetY = (int) target.coords.getY();
        int targetX = (int) target.coords.getX();
        int jUp = targetY;
        int jDown = targetY;
        int iUp = targetX;
        int iDown = targetX;
        int i = 0; // Distance from target in x.
        int j = 0; // Distance from target in y.
        // ArrayList of Workspace that need to be checked after the loop exits.
        ArrayList<Workspace> toCheck = new ArrayList<>();
        Workspace check;
        while ((jUp < gridSize || jDown >= 0 ||
                iUp < gridSize || iDown >= 0) && toCheck.isEmpty()) {
            // region Check the workspace that is up in the y direction.
            iUp = targetX;
            iDown = targetX;
            jUp = targetY + j;
            while ((iUp <= targetX + i || iDown >= targetX - i) &&
                    jUp < gridSize) {
                // Check the workspace that is up in x direction.
                if (iUp < gridSize) {
                    check = getLowestWorkspace(new Point(iUp, jUp));
                    if (check.nodeDensity > 0) {
                        toCheck.add(check);
                    }
                }
                // Check the workspace that is down in x direction.
                if (iDown >= 0) {
                    check = getLowestWorkspace(new Point(iDown, jUp));
                    if (check.nodeDensity > 0) {
                        toCheck.add(check);
                    }
                }
                iUp++;
                iDown--;
            }
            // endregion
            // region Check the workspace that is down in the y direction.
            iUp = targetX;
            iDown = targetX;
            jDown = targetY - j;
            while ((iUp <= targetX + i || iDown >= targetX - i) &&
                    jDown >= 0) {
                // Check the workspace that is up in x direction.
                if (iUp < gridSize) {
                    check = getLowestWorkspace(new Point(iUp, jDown));
                    if (check.nodeDensity > 0) {
                        toCheck.add(check);
                    }
                }
                // Check the workspace that is down in x direction.
                if (iDown >= 0) {
                    check = getLowestWorkspace(new Point(iDown, jDown));
                    if (check.nodeDensity > 0) {
                        toCheck.add(check);
                    }
                }
                iUp++;
                iDown--;
            }
            //endregion
            // region Check the workspace that is up in the x direction.
            jUp = targetY;
            jDown = targetY;
            iUp = targetX + i;
            while ((jUp <= targetY + j || jDown >= targetY - j) &&
                    iUp < gridSize) {
                // Check the workspace that is up in y direction.
                if (jUp < gridSize) {
                    check = getLowestWorkspace(new Point(iUp, jUp));
                    if (check.nodeDensity > 0) {
                        toCheck.add(check);
                    }
                }
                // Check the workspace that is down in y direction.
                if (jDown >= 0) {
                    check = getLowestWorkspace(new Point(iUp, jDown));
                    if (check.nodeDensity > 0) {
                        toCheck.add(check);
                    }
                }
                jUp++;
                jDown--;
            }
            //endregion
            // region Check the workspace that is down in the x direction.
            jUp = targetY;
            jDown = targetY;
            iDown = targetX - i;
            while ((jUp <= targetY + j || jDown >= targetY - j) &&
                    iDown >= 0) {
                // Check the workspace that is up in y direction.
                if (jUp < gridSize) {
                    check = getLowestWorkspace(new Point(iDown, jUp));
                    if (check.nodeDensity > 0) {
                        toCheck.add(check);
                    }
                }
                // Check the workspace that is down in y direction.
                if (jDown >= 0) {
                    check = getLowestWorkspace(new Point(iDown, jDown));
                    if (check.nodeDensity > 0) {
                        toCheck.add(check);
                    }
                }
                jUp++;
                jDown--;
            }
            //endregion
            i++;
            j++;
            iUp = targetX + i;
            iDown = targetX - i;
            jUp = targetY + j;
            jDown = targetY - j;
        }

        // Iterate through toCheck to find the closest node.
        double closestDist = 1.42; // sqrt(2)
        ArmConfig closest = null;
        double dist;
        for (Workspace w : toCheck) {
            for (ArmConfig a : w.nodes) {
                // Calculate the distance.
                dist = a.getBaseCenter().distance(goalBase);
                if (dist < closestDist) {
                    closestDist = dist;
                    closest = a;
                }
            }
        }
        if(closest == null) {
            closest = nodes.get(new Random().nextInt(nodes.size()));
        }
        return closest;
    }

    /**
     * Gets the obstacle density from the workspace.
     */
    public int getObstacleDensity(Point2D point) {
        return getLowestWorkspace(point).obstacleDensity;
    }

    /**
     * Helper method that gets the isLowest level of workspace for this that
     * encloses point.
     */
    private Workspace getLowestWorkspace(Point2D point) {
        for (Workspace child : children) {
            if (child.contains(point) && child.isLowest) {
                return child;
            } else if (child.contains(point)) {
                return child.getLowestWorkspace(point);
            }
        }
        return null;
    }

    /**
     * Gets the lowest workspace at the coordinates.
     */
    private Workspace getLowestWorkspace(Point p) {
        double x = length * (p.getX() + 0.5);
        double y = length * (p.getY() + 0.5);

        return getLowestWorkspace(new Point2D.Double(x, y));
    }

    /**
     * Checks whether a workspace contains a point.
     */
    private boolean contains(Point2D point) {
        if (p1.getX() <= point.getX() && p2.getX() >= point.getX() &&
                p1.getY() <= point.getY() && p2.getY() >= point.getY()) {
            return true;
        }
        return false;
    }

    /**
     * Creates the children of this.
     */
    private void createChildren() {
        // Lengths of the children.
        double length = getLength() / 2;
        // Bottom left child.
        children.add(new Workspace(this, length,
                new Point2D.Double(p1.getX(), p1.getY())));
        // Top left child.
        children.add(new Workspace(this, length,
                new Point2D.Double(p1.getX(), p1.getY() + length)));
        // Bottom right child.
        children.add(new Workspace(this, length,
                new Point2D.Double(p1.getX() + length, p1.getY())));
        // Top right child.
        children.add(new Workspace(this, length,
                new Point2D.Double(p1.getX() + length, p1.getY() + length)));
    }

    /**
     * Adds an obstacle to this. This will then find all of the isLowest level
     * children that intersect with obstacle. Returns true if obstacle was
     * successfully added.
     */
    public void addObstacle(Obstacle obstacle) {
        // Check whether we are at the isLowest level of the hierarchy. If so
        // we automatically add obstacle.
        if (isLowest) {
            obstacles.add(obstacle);
            hasObstacles = true;
            obstacleDensity++;
        }
        // To determine whether this obstacle intersects with a workspace
        // treat the workspace as a rectangle and see if obstacle intersects
        // with the Rectangle representation of it.
        for (Workspace child : children) {
            Rectangle2D rect = child.getRect();
            if (obstacle.checkCollision(rect)) {
                child.addObstacle(obstacle);
                hasObstacles = true;
            }
        }
    }

    /**
     * Checks for a collision between point and the obstacles in this
     * workspace. Returns true if there is a collision.
     */
    public boolean checkCollision(Point2D point) {
        // Check if this is the isLowest level and if so check point against
        // the obstacles in this.
        if (isLowest) {
            for (Obstacle obstacle : obstacles) {
                if (obstacle.checkCollision(point)) {
                    return true;
                }
            }
            return false;
        }
        // Check which of the children this point lies within using the
        // obstacle representation of each child.
        for (Workspace child : children) {
            Obstacle childAsWorkspace = child.getWorkspaceAsObstacle();
            if (childAsWorkspace.checkCollision(point)) {
                return child.checkCollision(point);
            }
        }
        return false;
    }

    /**
     * Checks for a collision between line and the obstacles in this
     * workspace. Returns true if there is a collision.
     */
    public boolean checkCollision(Line2D line) {
        // Check if this is the isLowest level and if so check point against
        // the obstacles in this.
        if (isLowest) {
            for (Obstacle obstacle : obstacles) {
                if (obstacle.checkCollision(line)) {
                    return true;
                }
            }
            return false;
        }
        // Check which of the children this point lies within using the
        // obstacle representation of each child.
        for (Workspace child : children) {
            Obstacle childAsWorkspace = child.getWorkspaceAsObstacle();
            if (child.hasObstacles) {
                if (childAsWorkspace.checkCollision(line)) {
                    if (child.checkCollision(line)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Gets the length of one of the sides of the workspace.
     */
    public double getLength() {
        return p2.getX() - p1.getX();
    }

    /**
     * Gets the rectangle representing this.
     */
    public Rectangle2D getRect() {
        return new Rectangle2D.Double(p1.getX(), p1.getY(),
                getLength(), getLength());
    }

    /**
     * Treating this workspace as an Obstacle, gets the Obstacle
     * representation of this.
     */
    public Obstacle getWorkspaceAsObstacle() {
        return new Obstacle(p1.getX(), p1.getY(),
                getLength(), getLength());
    }

    /**
     * Checks that the invariant is true.
     */
    public boolean checkInvariant() {
        return (p2.getX() - p1.getX()) == (p2.getY() - p1.getY());
    }
}
