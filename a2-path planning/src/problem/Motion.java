package problem;

import tester.Tester;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Used to generate a path.
 * <p>
 * Created by lachlan on 5/09/16.
 */
public class Motion {

    /**
     * Checks a move and returns goal if valid or throws an exception.
     */
    public static boolean checkMove(ArmConfig initial, ArmConfig goal,
                                    Tester tester) {

        // Required to check if the goal is in collision with obstacles,
        // boundaries or itself.
        if (goal.checkCollision(tester)) {
            return false;
        }

        ArrayList<Quadrant> iQuadrants = getQuadrants(initial);
        ArrayList<Quadrant> gQuadrants = getQuadrants(goal);
        // Create the simple rectangle around the ArmConfigs that will be
        // used to check collisions.
        Rectangle2D boundingRect = createSimpleBoundRect(iQuadrants, gQuadrants,
                initial, goal);
        // Create the rectangle around the ArmConfigs that will be used to
        // check collisions.

        // Use the rectangle to check if there is a collision. If so then
        // perform a full check. A raw check over all obstacles will be
        // faster then using the workspace.
        List<Obstacle> obstacles = tester.getProblemSpec().getObstacles();
        boolean fullCheck = false;
        for (Obstacle o : obstacles) {
            if (o.checkCollision(boundingRect)) {
                fullCheck = true;
                break;
            }
        }
        if (fullCheck) {
            try {
                move(initial, goal, tester);
            } catch (CollisionDetectedException e) {
                return false;
            }
        }
        return true;
    }

    // region checkMove support code.

    /**
     * Creates a simple rectangle that bounds two ArmConfigs for the worst case.
     */
    private static Rectangle2D createSimpleBoundRect(
            ArrayList<Quadrant> a1Quadrants, ArrayList<Quadrant> a2Quadrants,
            ArmConfig a1, ArmConfig a2) {
        // The rectangle starts as the rectangle around a1 and a2 and expands
        // out.
        Rectangle2D bR = createBaseBoundRect(a1, a2); // boundingRectangle

        // Get the maximum length that the arm can extend out from the center
        // of the base from the first joint.
        double maxLength = getMaxLength(a1, a2);

        // Check if all the joints are in the same quadrant.
        boolean sameQuadrants = true;
        int size = a1Quadrants.size();
        for (int i = 0; i < size; i++) {
            if (a1Quadrants.get(i) != a2Quadrants.get(i)) {
                sameQuadrants = false;
            }
        }
        // Determine the smaller bounds for the lengths.
        double maxWidth = 0.0;
        double maxHeight = 0.0;
        if (sameQuadrants) {
            maxWidth = a1.getMaximumWidth() > a2.getMaximumWidth() ?
                    a1.getMaximumWidth() : a2.getMaximumWidth();
            maxHeight = a1.getMaximumHeight() > a2.getMaximumHeight() ?
                    a1.getMaximumHeight() : a2.getMaximumHeight();
        }

        // Increase the size of boundingRect depending on the combination of
        // Quadrants that we have. Only use the first quadrant.
        Quadrant q1 = a1Quadrants.get(0);
        Quadrant q2 = a2Quadrants.get(0);
        if (q1 == Quadrant.TL && q2 == Quadrant.TL) {
            if (sameQuadrants) {
                bR = new Rectangle2D.Double(bR.getMinX() - maxWidth,
                        bR.getMinY(), maxWidth + bR.getWidth(),
                        maxHeight + bR.getHeight());
            } else {
                bR = new Rectangle2D.Double(bR.getMinX() - maxLength,
                        bR.getMinY(), maxLength + bR.getWidth(),
                        maxLength + bR.getHeight());
            }
        } else if (q1 == Quadrant.TR && q2 == Quadrant.TR) {
            bR = new Rectangle2D.Double(bR.getMinX(), bR.getMinY(),
                    maxLength + bR.getWidth(), maxLength + bR.getHeight());
        } else if (q1 == Quadrant.BR && q2 == Quadrant.BR) {
            bR = new Rectangle2D.Double(bR.getMinX(), bR.getMinY() - maxLength,
                    maxLength + bR.getWidth(), maxLength + bR.getHeight());
        } else if (q1 == Quadrant.BL && q2 == Quadrant.BL) {
            bR = new Rectangle2D.Double(bR.getMinX() - maxLength,
                    bR.getMinY() - maxLength, maxLength + bR.getWidth(),
                    maxLength + bR.getHeight());
        } else if ((q1 == Quadrant.TL && q2 == Quadrant.TR) ||
                (q2 == Quadrant.TL && q1 == Quadrant.TR)) {
            bR = new Rectangle2D.Double(bR.getMinX() - maxLength, bR.getMinY(),
                    maxLength + bR.getWidth(), 2 * maxLength + bR.getHeight());
        } else if ((q1 == Quadrant.TL && q2 == Quadrant.BL) ||
                (q1 == Quadrant.TL && q2 == Quadrant.BR) ||
                (q2 == Quadrant.TL && q1 == Quadrant.BL) ||
                (q2 == Quadrant.TL && q1 == Quadrant.BR)) {
            bR = new Rectangle2D.Double(bR.getMinX() - maxLength,
                    bR.getMinY() - maxLength, 2 * maxLength + bR.getWidth(),
                    2 * maxLength + bR.getHeight());
        } else if ((q1 == Quadrant.TR && q2 == Quadrant.BR) ||
                (q2 == Quadrant.TR && q1 == Quadrant.BR)) {
            bR = new Rectangle2D.Double(bR.getMinX(), bR.getMinY() - maxLength,
                    bR.getWidth() + maxLength, bR.getHeight() + 2 * maxLength);
        } else {
            bR = new Rectangle2D.Double(bR.getMinX() - maxLength,
                    bR.getMinY() - maxLength, 2 * maxLength + bR.getWidth(),
                    2 * maxLength + bR.getHeight());
        }
        // Expand the rectangle according to the actual bounding box.
        bR = checkRect(bR, a1, a2);
        return bR;
    }

    /**
     * Returns a rectangle that is guaranteed to be larger than any
     * configuration of the ArmConfig as it moves from initial to goal.
     */
    private static Rectangle2D checkRect(Rectangle2D bR, ArmConfig a1,
                                         ArmConfig a2) {
        Rectangle2D a1BR = a1.getBoundRect();
        Rectangle2D a2BR = a2.getBoundRect();
        double maxX = bR.getMaxX();
        double minX = bR.getMinX();
        double maxY = bR.getMaxY();
        double minY = bR.getMinY();
        if (a1BR.getMaxX() > maxX) {
            maxX = a1BR.getMaxX();
        }
        if (a2BR.getMaxX() > maxX) {
            maxX = a2BR.getMaxX();
        }
        if (a1BR.getMinX() < minX) {
            minX = a1BR.getMinX();
        }
        if (a2BR.getMinX() < minX) {
            minX = a2BR.getMinX();
        }
        if (a1BR.getMaxY() > maxY) {
            maxY = a1BR.getMaxY();
        }
        if (a2BR.getMaxY() > maxY) {
            maxY = a2BR.getMaxY();
        }
        if (a1BR.getMinY() < minY) {
            minY = a1BR.getMinY();
        }
        if (a2BR.getMinY() < minY) {
            minY = a2BR.getMinY();
        }
        bR = new Rectangle2D.Double(minX, minY, maxX - minX, maxY - minY);
        return bR;
    }

    /**
     * Gets the max length that the arms can extend from the base between two
     * configurations.
     */
    private static double getMaxLength(ArmConfig a1, ArmConfig a2) {
        double maxLength1 = a1.getMaxLength();
        double maxLength2 = a2.getMaxLength();
        return (maxLength1 > maxLength2) ? maxLength1 : maxLength2;
    }

    /**
     * Creates a rectangle that bounds the two ArmConfig bases.
     */
    private static Rectangle2D createBaseBoundRect(ArmConfig a1, ArmConfig a2) {
        Rectangle2D a1Rect = a1.getChairAsRect();
        Rectangle2D a2Rect = a2.getChairAsRect();

        double maxX = a1Rect.getMaxX();
        double minX = a1Rect.getMinX();
        double maxY = a1Rect.getMaxY();
        double minY = a1Rect.getMinY();

        if (maxX < a2Rect.getMaxX()) {
            maxX = a2Rect.getMaxX();
        }
        if (minX > a2Rect.getMinX()) {
            minX = a2Rect.getMinX();
        }
        if (maxY < a2Rect.getMaxY()) {
            maxY = a2Rect.getMaxY();
        }
        if (minY > a2Rect.getMinY()) {
            minY = a2Rect.getMinY();
        }

        return new Rectangle2D.Double(minX, minY, maxX - minX, maxY - minY);
    }

    /**
     * Gets the quadrants that each of the joint angles lie in for the given
     * ArmConfig.
     */
    private static ArrayList<Quadrant> getQuadrants(ArmConfig a) {
        ArrayList<Quadrant> quadrants = new ArrayList<>();
        for (double angle : a.getJointAngles()) {
            if (angle > Math.PI / 2) {
                quadrants.add(Quadrant.TL);
            } else if (angle > 0) {
                quadrants.add(Quadrant.TR);
            } else if (angle > -Math.PI / 2) {
                quadrants.add(Quadrant.BR);
            } else {
                quadrants.add(Quadrant.BL);
            }
        }
        return quadrants;
    }

    // endregion

    /**
     * Given a desired end node, determines the primitive steps to reach the
     * node from the given configuration. The node has been reached when the
     * base is on the centre of the goal. An exception is thrown if the
     * goal cannot be reached without collision.
     *
     * @require the base of current is not in collision at goal && distance
     * between the base of current and goal is less than a primitive move.
     */

    public static ArrayList<ArmConfig> move(ArmConfig initial, ArmConfig goal,
                                            Tester tester)
            throws CollisionDetectedException {

        // Required to check if the goal is in collision with obstacles,
        // boundaries or itself.
        if (goal.checkCollision(tester)) {
            throw new CollisionDetectedException("Collision in goal " +
                    "configuration.");
        }

        // Determine the primitive moves to be made each step by the base.
        Point2D delta = calculateDelta(initial, goal);

        // The path to be return.
        ArrayList<ArmConfig> path = new ArrayList<>();
        path.add(initial);

        // Iteratively make primitive moves until either we are at the goal
        // or have encountered a collision.
        ArmConfig current = initial;
        while (!current.isSameConfig(goal)) {
            // Move the base and all the joints.
            // For testing comment out the following lines and comment in the
            // try-catch statement. Functionality does not change but a file
            // is created for a failed path to save it.
            current = movePrimitive(current, goal, delta, tester);
            path.add(0, current);
//            try {
//                current = movePrimitive(current, goal, delta, tester);
//                path.add(current);
//            } catch(CollisionDetectedException e) {
//                tester.getProblemSpec().setPath(path);
//                try {
//                    tester.getProblemSpec().saveSolution(
//                            "./testcases/motion_fail_" + path.size() + "
// .txt");
//                } catch(IOException ex) {
//                    System.out.println("Failed saving failed path.");
//                }
//                throw new CollisionDetectedException(e.getMessage());
//            }
        }

        tester.getProblemSpec().setPath(path);
        List<Integer> collisions = tester.getCollidingStates();
        if(collisions.size() != 0) {
            try {
                tester.getProblemSpec().saveSolution("./testcases/all_fails" +
                        ".txt");
            } catch (IOException e) {
                System.out.println("Couldn't save fails.");
            }
            System.out.println("We fuck up here.");

        }

        return path;
    }

    // region move support code.

    /**
     * Makes a primitive move towards the goal.
     */
    private static ArmConfig movePrimitive(ArmConfig current, ArmConfig goal,
                                           Point2D delta, Tester tester)
            throws CollisionDetectedException {
        ArmConfig move;
        // Move the base by delta.
        move = moveBase(current, delta, goal.getBaseCenter());
        // Rotate the joints towards goal.
        move = moveJoints(move, goal);
        // If there are grippers, move the grippers.
        if (goal.hasGripper()) {
            move = manipulateGrippers(move, tester, goal);
        }
        // Perform a check at the end.
        if (move.checkCollision(tester)) {
            throw new CollisionDetectedException("Collision while in move " +
                    "algorithm at: " + move.getBaseCenter());
        }
        return move;
    }

    /**
     * Returns a new ArmConfig towards the goal by delta.
     */
    private static ArmConfig moveBase(ArmConfig current, Point2D delta,
                                      Point2D goal)
            throws CollisionDetectedException {
        Point2D currentBase = current.getBaseCenter();
        // Check if we're within a primitive move.
        if (getLength(new Point2D.Double(goal.getX() - currentBase.getX(),
                goal.getY() - currentBase.getY())) <= Tester.MAX_BASE_STEP) {
            return new ArmConfig(current, goal);
        }
        // Calculate the new base.
        Point2D newBase = new Point2D.Double(currentBase.getX() + delta.getX(),
                currentBase.getY() + delta.getY());
        return new ArmConfig(current, newBase);
    }

    /**
     * Returns a configuration where the joints have been manipulated
     * probabilistically either towards the overall goal or to a random
     * position.
     */
    private static ArmConfig moveJoints(ArmConfig current, ArmConfig goal) {
        ArrayList<Double> goalJoints = goal.getJointAngles();
        ArrayList<Double> currentJoints = current.getJointAngles();
        int numJoints = goalJoints.size();
        Double currentAngle;
        // Manipulate each of the joints.
        for (int i = 0; i < numJoints; i++) {
            currentAngle = currentJoints.get(i);
            currentJoints.set(i, determineAngle(currentAngle,
                    goalJoints.get(i)));
        }
        if (current.hasGripper()) {
            return new ArmConfig(current.getBaseCenter(), currentJoints,
                    current.getGripperLengths());
        } else {
            return new ArmConfig(current.getBaseCenter(), currentJoints);
        }
    }

    /**
     * Determines the new angle based on the goal and current.
     */
    private static Double determineAngle(Double old, Double goal) {
        if (goal - old > Tester.MAX_JOINT_STEP) {
            return old + Tester.MAX_JOINT_STEP;
        } else if (old - goal > Tester.MAX_JOINT_STEP) {
            return old - Tester.MAX_JOINT_STEP;
        } else {
            return goal;
        }

    }

    /**
     * Manipulates the gripper to minimise the gripper lengths while we are a
     * certain distance from the goal configuration otherwise moves towards
     * the goal.
     *
     * @param goal the short term goal configuration of the RRT algorithm.
     */
    private static ArmConfig manipulateGrippers(ArmConfig current,
                                                Tester tester, ArmConfig goal) {
        return resizeGrippers(current, goal.getGripperLengths());
    }

    /**
     * Minimises the gripper's size if it is reasonable to do so. Returns
     * true if the grippers are at their smallest size and false if not. If a
     * change is made it will be checked and provided there is no collision
     * added to path.
     *
     * @require !current.checkCollision(workspace) && current.hasGrippers()
     */
    private static ArmConfig resizeGrippers(ArmConfig current,
                                            ArrayList<Double> goal) {
        
        ArrayList<Double> gripper = current.getGripperLengths();
        Double u1 = gripper.get(ArmConfig.u1);
        Double u2 = gripper.get(ArmConfig.u2);
        Double l1 = gripper.get(ArmConfig.l1);
        Double l2 = gripper.get(ArmConfig.l2);
        // Move all the grippers blindly towards the goal by the step.
        gripper.set(ArmConfig.u1,
                determineGripperLength(u1, goal.get(ArmConfig.u1)));
        gripper.set(ArmConfig.u2,
                determineGripperLength(u2, goal.get(ArmConfig.u2)));
        gripper.set(ArmConfig.l1,
                determineGripperLength(l1, goal.get(ArmConfig.l1)));
        gripper.set(ArmConfig.l2,
                determineGripperLength(l2, goal.get(ArmConfig.l2)));
        // Check if the new configuration is legal.
        return new ArmConfig(current.getBaseCenter(),
                current.getJointAngles(), gripper);
    }

    /**
     * Determines the new gripper length based on a goal.
     */
    private static Double determineGripperLength(Double old, Double goal) {
        Double maxStep = Tester.MAX_GRIPPER_STEP;
        if (old > goal + maxStep) {
            return old - maxStep;
        } else if (old < goal - maxStep) {
            return old + maxStep;
        } else {
            return goal;
        }
    }

    /**
     * Calculates the primitive x movement the base should make each step to
     * get to goal.
     *
     * @return the primitive step the base should make each step as a point
     * with the primitive x in the x coordinate of /return and likewise for y.
     */
    private static Point2D calculateDelta(ArmConfig initial, ArmConfig goal) {
        Point2D delta = getVector(initial.getBaseCenter(),
                goal.getBaseCenter());
        double delX = delta.getX();
        double delY = delta.getY();
        double length = getLength(delta);

        return new Point2D.Double(delX / length * Tester.MAX_BASE_STEP,
                delY / length * Tester.MAX_BASE_STEP);
    }

    /**
     * Gets the length of a vector.
     */
    private static double getLength(Point2D vector) {
        double delX = vector.getX();
        double delY = vector.getY();
        return Math.sqrt(delX * delX + delY * delY);
    }

    /**
     * Gets the vector from pointA to pointB.
     *
     * @return Returns a point that represents the vector from A to B.
     */
    private static Point2D getVector(Point2D pointA, Point2D pointB) {
        return new Point2D.Double(pointB.getX() - pointA.getX(),
                pointB.getY() - pointA.getY());
    }

    // endregion

    /**
     * Method to test minimise grippers.
     */
//    @Test
//    public void testMinimiseGrippers() {
//        Workspace w = new Workspace();
//        w.addObstacle(new Obstacle(0.8, 0.8, 0.1, 0.03));
//
//        ArrayList<ArmConfig> path = new ArrayList<>();
//
//        ArrayList<Double> joints = new ArrayList<>();
//        joints.add(0.0);
//        joints.add(0.0);
//        ArrayList<Double> lengths = new ArrayList<>();
//        for (int i = 0; i < 4; i++) {
//            lengths.add(0.03);
//        }
//        // Test a config with initially minimised joints.
//        ArmConfig initial = new ArmConfig(new Point2D.Double(0.5, 0.5),
//                joints, lengths);
//        Assert.assertTrue(resizeGrippers(initial, path, w));
//        Assert.assertTrue(path.size() == 0);
//        // Test a config with initially larger than minimum joints.
//        lengths = new ArrayList<>();
//        for (int i = 0; i < 4; i++) {
//            lengths.add(0.032);
//        }
//        initial = new ArmConfig(new Point2D.Double(0.5, 0.5), joints,
// lengths);
//        Assert.assertFalse(resizeGrippers(initial, path, w));
//        Assert.assertTrue(path.size() == 2);
//        Assert.assertTrue(resizeGrippers(path.get(path.size() - 1), path, w));
//        Assert.assertTrue(path.size() == 4);
//        // Reset path.
//        path = new ArrayList<>();
//        // Test getting out of a collision.
//        lengths = new ArrayList<>();
//        for (int i = 0; i < 4; i++) {
//            lengths.add(0.031);
//        }
//        initial = new ArmConfig(new Point2D.Double(0.75, 0.769),
//                joints, lengths);
//        Assert.assertTrue(initial.checkCollision(w));
//        Assert.assertTrue(resizeGrippers(initial, path, w));
//        Assert.assertTrue(path.size() == 2);
//        // Reset path.
//        path = new ArrayList<>();
//        // Test failing to get out of a collision where the obstacle is
//        // between the grippers (but not initially in collision).
//        lengths = new ArrayList<>();
//        for (int i = 0; i < 4; i++) {
//            lengths.add(0.031);
//        }
//        initial = new ArmConfig(new Point2D.Double(0.69, 0.8305),
//                joints, lengths);
//        Assert.assertFalse(initial.checkCollision(w));
//        Assert.assertFalse(resizeGrippers(initial, path, w));
//
//    }
}
