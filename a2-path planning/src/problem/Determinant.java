package problem;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 * Calculates the determinant of a given matrix. Adapted from Prof. Michael
 * Helbig's implementation on bigdev.de.
 * <p>
 * Created by lachlan on 4/09/16.
 */
public class Determinant {

    /**
     * Calculates the determinant of a given matrix of doubles. Defined
     * recursively. Returns the determinant if it exists or null if not.
     */
    public static Double determinant(double[][] matrix) {
        if (matrix.length != matrix[0].length) {
            return null;
        }
        int length = matrix.length;
        // Base case.
        if (length == 1) {
            return matrix[0][0];
        } else {
            double determinant = 0;
            for (int i = 0; i < length; i++) {
                determinant += Math.pow(-1, i) * matrix[0][i] *
                        determinant(minor(matrix, i));
            }
            return determinant;
        }
    }

    /**
     * Generates a matrix that is built from matrix with row 0 and i removed.
     *
     * @require matrix must be square.
     */
    public static double[][] minor(double[][] matrix, int i) {
        int length = matrix.length;
        double[][] output = new double[length - 1][length - 1];
        for (int j = 1; j < length; j++) {
            int newCounter = 0;
            for (int m = 0; m < length; m++) {
                if (!(m == i)) {
                    output[j - 1][newCounter] = matrix[j][m];
                    newCounter++;
                }
            }
        }
        return output;
    }

    /**
     * Returns an int to indicate the sign of the area of the three points
     * according to the determinant method:
     * area = det({ax,ay,1},{bx,by,1},{cx,cy,1}). If the determinant is
     * within the error it will be declared zero to indicate that the points
     * are approximately co-linear.
     */
    public static int orientation(Point2D a, Point2D b, Point2D c,
                                  double error) {
        double[][] matrix = {{a.getX(), a.getY(), 1.0},
                {b.getX(), b.getY(), 1}, {c.getX(), c.getY(), 1}};
        double determinant = Determinant.determinant(matrix);
        if (determinant > -error && determinant < error) {
            return 0;
        } else if (determinant > error) {
            return 1;
        } else {
            return -1;
        }
    }

    /**
     * Returns an int indicating whether point is to the right (positive),
     * left (negative) or coaxial (zero) measured from the leftmost point of
     * the line to the rightmost point.
     *
     * Uses an error of 1e-18 to avoid the double rounding problems.
     *
     * @return 1 if the point is to the left of the line and -1 if it is to
     * the right.
     */
    public static int rightLeft(Line2D line, Point2D point) {
        if (line.getP1().getX() < line.getP2().getX()) {
            return orientation(line.getP1(), line.getP2(), point, 1e-18);
        } else {
            return orientation(line.getP2(), line.getP1(), point, 1e-18);
        }
    }
}
