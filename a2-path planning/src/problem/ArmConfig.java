package problem;

import tester.Tester;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Represents a configuration of the arm, i.e. base x-y coordinates and joint
 * angles. This class doesn't do any validity checking.
 * Each arm configuration needs to keep track of the primitive moves made in
 * order
 * to get from its parent to its current configuration.
 */
public class ArmConfig /*implements Comparable<ArmConfig>*/ {

    private ArmConfig parent;
    private ArrayList<ArmConfig> children = new ArrayList<>();
    private ArrayList<ArmConfig> movesMade = new ArrayList<>();
    private double cost;

    /**
     * Length of each link
     */
    public static final double LINK_LENGTH = 0.05;
    /**
     * Width of chair
     */
    public static final double CHAIR_WIDTH = 0.04;
    /**
     * Index of gripper links in gripperLengths
     */
    public static final int u1 = 0;
    public static final int u2 = 1;
    public static final int l1 = 2;
    public static final int l2 = 3;

    /**
     * Base x and y coordinates
     */
    private Point2D base;
    /**
     * Boundary of chair as Line2D
     */
    private List<Line2D> chair;
    /**
     * Boundary of the chair as Rectangle2D.
     */
    private Rectangle2D chairRect;
    /**
     * Joint angles in radians
     */
    private List<Double> jointAngles;
    /**
     * Links as Line2D
     */
    private List<Line2D> links;

    /**
     * Whether this configuration has a gripper
     */
    private boolean gripper;
    /**
     * Lengths of gripper segments
     */
    private List<Double> gripperLengths;

    /**
     * The maximum length the gripper can extend from the base centre.
     */
    private double maxLength = -1.0;


    /**
     * Constructor (no gripper)
     *
     * @param base        Base coordinates (Point2D)
     * @param jointAngles Joint angles in radians
     */
    public ArmConfig(Point2D base, List<Double> jointAngles) {
        this.base = new Point2D.Double(base.getX(), base.getY());
        this.jointAngles = new ArrayList<>(jointAngles);
        this.gripper = false;
        generateLinks();
        generateChair();

    }

    /**
     * Constructor (with gripper)
     *
     * @param base        Base coordinates (Point2D)
     * @param jointAngles Joint angles in radians
     */
    public ArmConfig(Point2D base, List<Double> jointAngles, List<Double>
            gripperLengths) {
        this.base = new Point2D.Double(base.getX(), base.getY());
        this.jointAngles = new ArrayList<>(jointAngles);
        this.gripperLengths = new ArrayList<>(gripperLengths);
        this.gripper = true;
        generateLinks();
        generateChair();
    }

    /**
     * Constructs an ArmConfig from a space-separated string. The first two
     * numbers are the x and y coordinates of the robot's base. The subsequent
     * numbers are the joint angles in sequential order defined in radians.
     *
     * @param str The String containing the values
     * @throws InputMismatchException
     */
    public ArmConfig(String str) throws InputMismatchException {
        Scanner s = new Scanner(str);
        base = new Point2D.Double(s.nextDouble(), s.nextDouble());
        jointAngles = new ArrayList<Double>();
        while (s.hasNextDouble()) {
            jointAngles.add(s.nextDouble());
        }
        s.close();
        this.gripper = false;
        generateLinks();
        generateChair();
    }

    /**
     * Constructs an ArmConfig from a space-separated string. The first two
     * numbers are the x and y coordinates of the robot's base. The subsequent
     * numbers are the joint angles in sequential order defined in radians.
     *
     * @param str The String containing the values
     * @throws InputMismatchException
     */
    public ArmConfig(String str, boolean gripper) throws
            InputMismatchException {

        this.gripper = gripper;

        Scanner s = new Scanner(str);
        base = new Point2D.Double(s.nextDouble(), s.nextDouble());
        jointAngles = new ArrayList<Double>();
        gripperLengths = new ArrayList<Double>();

        List<Double> tokens = new ArrayList<Double>();
        while (s.hasNextDouble()) {
            tokens.add(s.nextDouble());
        }

        if (this.gripper) {
            jointAngles.addAll(tokens.subList(0, tokens.size() - 4));
            gripperLengths.addAll(tokens.subList(tokens.size() - 4, tokens
                    .size()));
        } else {
            jointAngles = tokens;
        }

        s.close();
        generateLinks();
        generateChair();
    }

    /**
     * Copy constructor.
     *
     * @param cfg the configuration to copy.
     */
    public ArmConfig(ArmConfig cfg) {
        base = cfg.getBaseCenter();
        jointAngles = cfg.getJointAngles();
        links = cfg.getLinks();
        chair = cfg.getChair();
        gripper = cfg.hasGripper();
    }

    /**
     * Copy constructor using new base.
     */
    public ArmConfig(ArmConfig cfg, Point2D newBase) {
        base = newBase;
        jointAngles = cfg.getJointAngles();
        if (cfg.hasGripper()) {
            gripper = true;
            gripperLengths = cfg.getGripperLengths();
        } else {
            gripper = false;
        }
        generateLinks();
        generateChair();
    }

    /**
     * Check whether the arm configuration is colliding with obstacles, the
     * workspace boundary or itself.
     *
     * @return true if there is a collision with an obstacle, this is outside
     * the workspace boundary or collides with itself.
     */
    public boolean checkCollision(Tester tester) {
        // Check if it is within the boundary.
        if (!tester.fitsBounds(this)) {
            return true;
        }
        // Check for self collision.
        if (tester.hasSelfCollision(this)) {
            return true;
        }
        // Check for obstacle collision.
        if (checkCollision(tester.getWorkspace())) {
            return true;
        }
        return false;
    }

    /**
     * Check whether the arm configuration is colliding with obstacles.
     */
    public boolean checkCollision(Workspace workspace) {
        if (checkCollisionLinks(workspace)) {
            return true;
        }
        if (checkCollisionBase(workspace)) {
            return true;
        }
        return false;
    }

    /**
     * Check whether the links are colliding with obstacles.
     */
    public boolean checkCollisionLinks(Workspace workspace) {
        // Check collisions with the links.
        for (Line2D link : links) {
            if (workspace.checkCollision(link)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check whether the base is colliding with obstacles.
     */
    public boolean checkCollisionBase(Workspace workspace) {
        // Check collisions with the base.
        for (Line2D link : chair) {
            if (workspace.checkCollision(link)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns a space-separated string representation of this configuration.
     *
     * @return a space-separated string representation of this configuration.
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(base.getX());
        sb.append(" ");
        sb.append(base.getY());
        for (Double angle : jointAngles) {
            sb.append(" ");
            sb.append(angle);
        }
        if (gripper) {
            for (Double gripper : gripperLengths) {
                sb.append(" ");
                sb.append(gripper);
            }
        }
        return sb.toString();
    }

    /**
     * Returns the number of joints in this configuration.
     *
     * @return the number of joints in this configuration.
     */
    public int getJointCount() {
        return jointAngles.size();
    }

    /**
     * Returns the base position.
     *
     * @return the base position.
     */
    public Point2D getBaseCenter() {
        return new Point2D.Double(base.getX(), base.getY());
    }

    /**
     * Returns the list of joint angles in radians.
     *
     * @return the list of joint angles in radians.
     */
    public ArrayList<Double> getJointAngles() {
        return new ArrayList<>(jointAngles);
    }

    /**
     * Returns whether the arm has a gripper.
     *
     * @return whether the arm has a gripper.
     */
    public boolean hasGripper() {
        return gripper;
    }

    /**
     * Returns the list of gripper lengths.
     *
     * @return the list of gripper lengths.
     */
    public ArrayList<Double> getGripperLengths() {
        return new ArrayList<Double>(gripperLengths);
    }

    /**
     * Returns the list of links as Line2D.
     *
     * @return the list of links as Line2D.
     */
    public List<Line2D> getLinks() {
        return new ArrayList<Line2D>(links);
    }

    /**
     * Returns the chair boundary as list of Line2D.
     *
     * @return the chair boundary as list of Line2D.
     */
    public List<Line2D> getChair() {
        return new ArrayList<Line2D>(chair);
    }

    /**
     * Returns the chair as a rectangle.
     */
    public Rectangle2D getChairAsRect() {
        return chairRect;
    }

    /**
     * Returns a Rectangle2D that bounds this.
     */
    public Rectangle2D getBoundRect() {
        Rectangle2D rect = getChairAsRect();
        double maxX = rect.getMaxX();
        double minX = rect.getMinX();
        double maxY = rect.getMaxY();
        double minY = rect.getMinY();

        for (Line2D link : links) {
            // region Set limits.
            if (link.getX1() > maxX) {
                maxX = link.getX1();
            }
            if (link.getX2() > maxX) {
                maxX = link.getX2();
            }
            if (link.getX1() < minX) {
                minX = link.getX1();
            }
            if (link.getX2() < minX) {
                minX = link.getX2();
            }
            if (link.getY1() > maxY) {
                maxY = link.getY1();
            }
            if (link.getY2() > maxY) {
                maxY = link.getY2();
            }
            if (link.getY1() < minY) {
                minY = link.getY1();
            }
            if (link.getY2() < minY) {
                minY = link.getY2();
            }
            // endregion
        }

        return new Rectangle2D.Double(minX, minY, maxX - minX, maxY - minY);
    }

    /**
     * Returns the maximum length that the arm can extend from the edge of
     * the chair.
     */
    public double getMaxLength() {
        if (maxLength < 0.1) {
            double length = jointAngles.size() * LINK_LENGTH;
            if (gripper) {
                length += getGripperLinkLength();
            }
            length -= CHAIR_WIDTH / 2;
            maxLength = length;
        }
        return maxLength - CHAIR_WIDTH / 2;
    }

    /**
     * Gets a length that represents the furthest that a gripper can extend
     * from the last joint.
     */
    private double getGripperLinkLength() {
        double u1 = gripperLengths.get(this.u1);
        double u2 = gripperLengths.get(this.u2);
        double l1 = gripperLengths.get(this.l1);
        double l2 = gripperLengths.get(this.l2);

        double uLength = Math.sqrt(u1 * u1 + u2 * u2);
        double lLength = Math.sqrt(l1 * l1 + l2 * l2);

        if (uLength < lLength) {
            return lLength;
        } else {
            return uLength;
        }
    }

    /**
     * Returns the maximum width (x-direction) that the arm extends from the
     * edge of the chair.
     */
    public double getMaximumWidth() {
        double length = 0.0;
        double maxLength = 0.0;
        for (Double angle : jointAngles) {
            length += Tester.LINK_LENGTH * Math.cos(angle);
            if (Math.abs(length) > maxLength) {
                maxLength = length;
            }
        }
        if (gripper) {
            length += getGripperLinkLength() * Math.cos(
                    jointAngles.get(jointAngles.size() - 1));
            if (Math.abs(length) > maxLength) {
                maxLength = length;
            }
        }
        return maxLength - CHAIR_WIDTH / 2;
    }

    /**
     * Returns the maximum width (x-direction) that the arm extends from the
     * edge of the chair.
     */
    public double getMaximumHeight() {
        double length = 0.0;
        double maxLength = 0.0;
        for (Double angle : jointAngles) {
            length += Tester.LINK_LENGTH * Math.sin(angle);
            if (Math.abs(length) > maxLength) {
                maxLength = length;
            }
        }
        if (gripper) {
            length += getGripperLinkLength() * Math.sin(
                    jointAngles.get(jointAngles.size() - 1));
            if (Math.abs(length) > maxLength) {
                maxLength = length;
            }
        }
        return maxLength - CHAIR_WIDTH / 2;
    }

    /**
     * Returns true if this is the same configuration as other.
     */
    public boolean isSameConfig(ArmConfig other) {
        // Check base.
        if (!other.getBaseCenter().equals(this.getBaseCenter())) {
            return false;
        }
        // Check the joints.
        if (this.links.size() != other.links.size()) {
            return false;
        }
        for (int i = 0; i < links.size(); i++) {
            if (!linksEqual(this.links.get(i), other.links.get(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks whether two links are equal.
     */
    private boolean linksEqual(Line2D l1, Line2D l2) {
        return l1.getX1() == l2.getX1() && l1.getX2() == l2.getX2() &&
                l1.getY1() == l2.getY1() && l1.getY2() == l2.getY2();
    }

    /**
     * Returns the maximum straight-line distance between the link endpoints
     * in this state vs. the other state, or -1 if the link counts don't match.
     *
     * @param otherState the other state to compare.
     * @return the maximum straight-line distance for any link endpoint.
     */
    public double maxDistance(ArmConfig otherState) {
        if (this.getJointCount() != otherState.getJointCount()) {
            return -1;
        }
        double maxDistance = base.distance(otherState.getBaseCenter());
        List<Line2D> otherLinks = otherState.getLinks();
        for (int i = 0; i < links.size(); i++) {
            double distance = links.get(i).getP2().distance(
                    otherLinks.get(i).getP2());
            if (distance > maxDistance) {
                maxDistance = distance;
            }
        }
        return maxDistance;
    }

    /**
     * Returns the total straight-line distance between the link endpoints
     * in this state vs. the other state, or -1 if the link counts don't match.
     *
     * @param otherState the other state to compare.
     * @return the total straight-line distance over all link endpoints.
     */
    public double totalDistance(ArmConfig otherState) {
        if (this.getJointCount() != otherState.getJointCount()) {
            return -1;
        }
        if (this.hasGripper() != otherState.hasGripper()) {
            return -1;
        }
        double totalDist = base.distance(otherState.getBaseCenter());
        List<Line2D> otherLinks = otherState.getLinks();
        for (int i = 0; i < links.size(); i++) {
            totalDist += links.get(i).getP2().distance(
                    otherLinks.get(i).getP2());
        }
        return totalDist;
    }

    /**
     * Returns the maximum difference in angle between the joints in this state
     * vs. the other state, or -1 if the joint counts don't match.
     *
     * @param otherState the other state to compare.
     * @return the maximum joint angle change for any joint.
     */
    public double maxAngleDiff(ArmConfig otherState) {
        if (this.getJointCount() != otherState.getJointCount()) {
            return -1;
        }
        List<Double> otherJointAngles = otherState.getJointAngles();
        double maxDiff = 0;
        for (int i = 0; i < jointAngles.size(); i++) {
            double diff = Math.abs(jointAngles.get(i) - otherJointAngles.get
                    (i));
            if (diff > maxDiff) {
                maxDiff = diff;
            }
        }
        return maxDiff;
    }

    /**
     * Returns the maximum difference in gripper lengths between this state
     * vs. the other state, or 0 if the configuration has no gripper.
     *
     * @param otherState the other state to compare.
     * @return the maximum gripper length change for any joint.
     */
    public double maxGripperDiff(ArmConfig otherState) {
        if (!gripper) {
            return 0.0;
        }

        List<Double> otherGripperLengths = otherState.getGripperLengths();
        double maxDiff = 0;
        for (int i = 0; i < gripperLengths.size(); i++) {
            double diff = Math.abs(gripperLengths.get(i) -
                    otherGripperLengths.get(i));
            if (diff > maxDiff) {
                maxDiff = diff;
            }
        }
        return maxDiff;
    }

    /**
     * Generates links from joint angles
     */
    private void generateLinks() {
        links = new ArrayList<Line2D>();
        double x1 = base.getX();
        double y1 = base.getY();
        double totalAngle = 0;
        for (Double angle : jointAngles) {
            totalAngle += angle;
            double x2 = x1 + LINK_LENGTH * Math.cos(totalAngle);
            double y2 = y1 + LINK_LENGTH * Math.sin(totalAngle);
            links.add(new Line2D.Double(x1, y1, x2, y2));
            x1 = x2;
            y1 = y2;
        }

        if (gripper) {
            // add link for u1
            double x2 = (gripperLengths.get(0) * Math.cos(totalAngle + (Math
                    .PI / 2))) + x1;
            double y2 = (gripperLengths.get(0) * Math.sin(totalAngle + (Math
                    .PI / 2))) + y1;
            links.add(new Line2D.Double(x1, y1, x2, y2));

            // add link for u2
            double x3 = (gripperLengths.get(1) * Math.cos(totalAngle)) + x2;
            double y3 = (gripperLengths.get(1) * Math.sin(totalAngle)) + y2;
            links.add(new Line2D.Double(x2, y2, x3, y3));

            // add link for l1
            x2 = (gripperLengths.get(2) * Math.cos(totalAngle - (Math.PI / 2)
            )) + x1;
            y2 = (gripperLengths.get(2) * Math.sin(totalAngle - (Math.PI / 2)
            )) + y1;
            links.add(new Line2D.Double(x1, y1, x2, y2));

            // add link for l2
            x3 = (gripperLengths.get(3) * Math.cos(totalAngle)) + x2;
            y3 = gripperLengths.get(3) * Math.sin(totalAngle) + y2;
            links.add(new Line2D.Double(x2, y2, x3, y3));
        }
    }

    private void generateChair() {
        chair = new ArrayList<Line2D>();
        double x1 = base.getX();
        double y1 = base.getY();
        double halfWidth = CHAIR_WIDTH / 2;

        // generate in clockwise order (for bounds check)
        chair.add(new Line2D.Double(x1 - halfWidth, y1 + halfWidth, x1 +
                halfWidth, y1 + halfWidth));
        chair.add(new Line2D.Double(x1 + halfWidth, y1 + halfWidth, x1 +
                halfWidth, y1 - halfWidth));
        chair.add(new Line2D.Double(x1 + halfWidth, y1 - halfWidth, x1 -
                halfWidth, y1 - halfWidth));
        chair.add(new Line2D.Double(x1 - halfWidth, y1 - halfWidth, x1 -
                halfWidth, y1 + halfWidth));

        // generate the chair as a rectangle
        chairRect = new Rectangle2D.Double(x1 - halfWidth, y1 - halfWidth,
                CHAIR_WIDTH, CHAIR_WIDTH);
    }

    public double getCost() {
        return this.cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void setParent(ArmConfig parent) {
        this.parent = parent;
    }

    public ArmConfig getParent() {
        return this.parent;
    }

    public void addChild(ArmConfig child) {
        this.children.add(child);
    }

    public ArrayList<ArmConfig> getMovesMade() {
        return this.movesMade;
    }

    public void setMovesMade(ArrayList<ArmConfig> movesMade) {
        this.movesMade = movesMade;
    }


    //// You must override hashCode and equals
    //@Override
    //public boolean equals(Object other) {
    //    boolean result = false;
    //    if (other instanceof ArmConfig) {
    //        ArmConfig armConfig = (ArmConfig) other;
    //        result = this.getCost() == ((ArmConfig) other).getCost();
    //    }
    //    return result;
    //}

    //@Override
    //public int hashCode() {
    //    final int prime = 41;
    //    int result = 41 * 65537 + (int) Math.round(cost);
    //    return result;
    //}

    //@Override
    //public int compareTo(ArmConfig other) {
    //    if (this.getCost() < other.getCost()) {
    //        return -1;
    //    } else if (this.getCost() > other.getCost()) {
    //        return 1;
    //    }

    //    return 0;
    //}

}