package problem;

import java.util.PriorityQueue;

/**
 * The priority queue that determines from which node on the
 * tree we will expand on.
 *
 * We want to expand the node with the least cost.
 *
 * The cost function consists of exploitation, obstacles and
 * exploration variables. This cost function is
 * defined in the SolutionGenerator class
 * Created by lachlan on 8/09/16.
 */


public class ArmConfigPriorityQueue {

   PriorityQueue<ArmConfig> priorityQueue = new PriorityQueue();

}
