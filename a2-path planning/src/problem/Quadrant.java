package problem;

/**
 * Created by lachlan on 24/09/16.
 *
 * Describes the quadrant that the angle of a joint lies in.
 */
public enum Quadrant {
    // Top-Left, Top-Right, Bottom-Left, Bottom_Right
    TL, TR, BL, BR
}
