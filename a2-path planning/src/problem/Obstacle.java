package problem;

import tester.Tester;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This class represents one of the rectangular obstacles in Assignment 1.
 *
 * @author lackofcheese
 */
public class Obstacle {
    /**
     * Stores the obstacle as a Rectangle2D
     */
    private Rectangle2D rect;

    /**
     * The error that the rectangle should be bounded by. This will be
     * included in the initialisation of rect.
     */
    private final static double error = 1e-5;

    /**
     * Constructs an obstacle with the given (x,y) coordinates of the
     * bottom-left corner, as well as the width and height.
     *
     * @param x the minimum x-value.
     * @param y the minimum y-value.
     * @param w the width of the obstacle.
     * @param h the height of the obstacle.
     */
    public Obstacle(double x, double y, double w, double h) {
        this.rect = new Rectangle2D.Double(x - error, y - error, w + 2 * error,
                h + 2 * error);
    }

    /**
     * Constructs an obstacle from the representation used in the input file:
     * that is, the x- and y- coordinates of the upper-left vertex followed by
     * the lower-right vertex
     *
     * @param str
     */
    public Obstacle(String str) {
        Scanner s = new Scanner(str);
        double xMin = s.nextDouble() - error;
        double yMax = s.nextDouble() + error;
        double xMax = s.nextDouble() + error;
        double yMin = s.nextDouble() - error;
        this.rect = new Rectangle2D.Double(xMin, yMin,
                xMax - xMin, yMax - yMin);
        s.close();
    }

    /**
     * Constructs an obstacle from the representation used in the input file:
     * that is, the x- and y- coordinates of the upper-left vertex followed by
     * the lower-right vertex
     *
     * @param str
     */
    public Obstacle(String str, double error) {
        Scanner s = new Scanner(str);
        double xMin = s.nextDouble() - error;
        double yMax = s.nextDouble() + error;
        double xMax = s.nextDouble() + error;
        double yMin = s.nextDouble() - error;
        this.rect = new Rectangle2D.Double(xMin, yMin,
                xMax - xMin, yMax - yMin);
        s.close();
    }

    /**
     * Returns a copy of the Rectangle2D representing this obstacle.
     *
     * @return a copy of the Rectangle2D representing this obstacle.
     */
    public Rectangle2D getRect() {
        return (Rectangle2D) rect.clone();
    }

    /**
     * Checks for a collision between this and point. Returns true if
     * collision occurs.
     */
    public boolean checkCollision(Point2D point) {
        // Boundary check.
//        if (rect.getMaxX() >= point.getX() && rect.getMinX() <= point.getX
// () &&
//                rect.getMaxY() >= point.getY() &&
//                rect.getMinY() <= point.getY()) {
//            return true;
//        }
//        return false;
        return this.rect.contains(point);
    }

    /**
     * Checks for a collision between this and obstacle. Returns true if
     * collision occurs or rect encloses this.
     */
    public boolean checkCollision(Rectangle2D rect) {
        return rect.intersects(this.rect);
//        // Check if the boundaries of rect enclose this.
//        if (rect.getMaxX() > this.rect.getMaxX() &&
//                rect.getMaxY() > this.rect.getMaxY() &&
//                rect.getMinX() < this.rect.getMinX() &&
//                rect.getMinY() < this.rect.getMinY()) {
//            return true;
//        }
//        // Turn rect into lines.
//        ArrayList<Line2D> lines = new ArrayList<>();
//        lines.add(new Line2D.Double(rect.getMinX(), rect.getMinY(),
//                rect.getMaxX(), rect.getMinY()));
//        lines.add(new Line2D.Double(rect.getMinX(), rect.getMaxY(),
//                rect.getMaxX(), rect.getMaxY()));
//        lines.add(new Line2D.Double(rect.getMinX(), rect.getMinY(),
//                rect.getMinX(), rect.getMaxY()));
//        lines.add(new Line2D.Double(rect.getMaxX(), rect.getMinY(),
//                rect.getMaxX(), rect.getMaxY()));
//        for (Line2D line : lines) {
//            if (checkCollision(line)) {
//                return true;
//            }
//        }
//        return false;
    }

    /**
     * Checks for a collision between this and line. Returns true if
     * collision occurs.
     */
    public boolean checkCollision(Line2D line) {
        Rectangle2D rect = this.getRect();
        return rect.intersectsLine(line);
//        double rectMaxX = rect.getMaxX();
//        double rectMinX = rect.getMinX();
//        double rectMaxY = rect.getMaxY();
//        double rectMinY = rect.getMinY();
//        double p1X = line.getP1().getX();
//        double p2X = line.getP2().getX();
//        double p1Y = line.getP1().getY();
//        double p2Y = line.getP2().getY();
//        // Boundary check for guaranteed misses.
//        if (p1X > rectMaxX && p2X > rectMaxX) {
//            return false;
//        }
//        if (p1X < rectMinX && p2X < rectMinX) {
//            return false;
//        }
//        if (p1Y > rectMaxY && p2Y > rectMaxY) {
//            return false;
//        }
//        if (p1Y < rectMinY && p2Y < rectMinY) {
//            return false;
//        }
//        // Boundary check for guaranteed collisions.
//        if (checkCollision(line.getP1())) {
//            return true;
//        }
//        if (checkCollision(line.getP2())) {
//            return true;
//        }
//        // Rigorous check using the rectangle's lines.
//        ArrayList<Line2D> rectLines = new ArrayList<>();
//        rectLines.add(new Line2D.Double(rectMaxX, rectMaxY, rectMaxX,
//                rectMinY));
//        rectLines.add(new Line2D.Double(rectMinX, rectMaxY, rectMinX,
//                rectMinY));
//        rectLines.add(new Line2D.Double(rectMaxX, rectMaxY, rectMinX,
//                rectMaxY));
//        rectLines.add(new Line2D.Double(rectMaxX, rectMinY, rectMinX,
//                rectMinY));
//        for (Line2D rectLine : rectLines) {
//            if (checkCollision(rectLine, line)) {
//                return true;
//            }
//        }
//        return false;
    }

    /**
     * Returns a String representation of this obstacle.
     *
     * @return a String representation of this obstacle.
     */
    public String toString() {
        return rect.toString();
    }
}
